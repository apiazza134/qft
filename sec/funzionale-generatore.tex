\section{Funzionale generatore}

Il path integral è un modo per calcolare le funzioni di Green
\begin{equation}
    \braket{\phi', t'| T\{\phi(x_{1}) \cdots \phi(x_{n})\} | \phi, t} = \int\limits_{\substack{\phi(t, \vec{x}) = \phi(\vec{x}) \\  \phi(t', \vec{x}) = \phi'(\vec{x})}} [\dif \phi] \, \phi(x_{1}) \cdots \phi(x_{n}) \, e^{i S[\phi, t', t]}
\end{equation}
dove di solito
\begin{equation}
    S[\phi, t', t] = \int_{t}^{t'} \dif{\tau} \int_{\R^{D-1}} \dif[D-1]{\vec{x}} \, \mathscr{L}(\phi(x), \partial_{\mu} \phi(x))
\end{equation}

La \emph{formula di Gell-Mann e Low} ci dà
\begin{equation}
    \braket{\Omega | T\{\phi(t_{1}) \cdots \phi(t_{n})\} | \Omega} = \lim_{\epsilon \to 0^{+}} \lim_{\substack{t' \to +\infty \\ t \to -\infty}} \frac{\braket{\phi', t' e^{-i\epsilon} | T\{\phi(x_{1}) \cdots \phi(x_{n})\} | \phi, t e^{-i\epsilon}}}{\braket{\phi', t' e^{-i\epsilon} | \phi, t e^{-i\epsilon}}}
\end{equation}
Ora il punto assolutamente non banale e che non ho capito se è quanto è vero (ma sembra essere quello che fanno tutti) è assumere che il limite di tale rapporto in cui i tempi hanno una piccola parte immaginaria sia uguale a usare il path integral in cui nel propagatore si tiene conto della prescrizione \( +i\epsilon \) di Feynman. Se assumiamo questo si ha allora
\begin{equation}
    \braket{\Omega | T\{\phi(x_{1}) \cdots \phi(x_{n})\} | \Omega} = \dfrac{\displaystyle \int [\dif \phi] \, \phi(x_{1}) \cdots \phi(x_{n}) \, e^{i S[\phi]}}{\displaystyle \int\limits [\dif \phi] \, e^{i S[\phi]}}
\end{equation}
dove ora
\begin{equation}
    S[\phi] = \int\dif[D]{x} \, \mathscr{L}(\phi(x), \partial_{\mu} \phi(x))
\end{equation}

\subsection{Funzionale \texorpdfstring{\( W \)}{W}}
Definiamo il funzionale generatore come
\begin{equation}
    Z[J] \coloneqq \int [\dif \phi] \, e^{i S[\phi] + i J \cdot \phi} \qquad J \cdot \phi = \int\dif[D]{x}\, J(x) \phi(x)
\end{equation}

Tale funzionale è il funzionale generatore delle funzioni di Green, nel senso che
\begin{equation}
    G^{(n)}(x_{1}, \ldots, x_{n}) = \braket{\Omega | T\{\phi(x_{1}) \cdots \phi(x_{n})\} | \Omega} = \frac{1}{i^{n}} \frac{1}{Z[0]} \left.\dfd{}{J(x_{1})} \cdots \dfd{}{J(x_{n})} Z[J]\right\vert_{J = 0}
\end{equation}
ovvero
\begin{equation}
    Z[J] = Z[0] \sum_{n\geq 0} \frac{i^{n}}{n!} \int G^{(n)}(x_{1}, \ldots, x_{n}) J(x_{1}) \cdots J(x_{n}) \dif[D]{x_{1}} \cdots \dif[D]{x_{n}}
\end{equation}
Definiamo ora il funzionale
\begin{equation}
    W[J] \coloneqq -i \log{Z[J]} \qquad Z[J] = e^{iW[J]}
\end{equation}
che per il \emph{teorema di esponenziazione} è il funzionale generatore delle funzioni di Green connesse
\begin{equation}
    G\ped{c}^{(n)}(x_{1}, \ldots, x_{n}) =  \frac{1}{i^{n}} \left.\dfd{}{J(x_{1})} \cdots \dfd{}{J(x_{n})} i W[J]\right\vert_{J = 0}
\end{equation}
o in modo equivalente
\begin{equation}
    iW[J] = \sum_{n\geq 0} \frac{i^{n}}{n!} \int G^{(n)}\ped{c}(x_{1}, \ldots, x_{n}) J(x_{1}) \cdots J(x_{n}) \dif[D]{x_{1}} \cdots \dif[D]{x_{n}}
\end{equation}

\subsection{Funzionale \texorpdfstring{\( \Gamma \)}{gamma}}
Definiamo l'azione efficace \( \Gamma \) come la trasformata di Legendre di \( W \). Sia quindi
\begin{equation}
    \varphi_{J}(x) = \dfd{W}{J(x)}
\end{equation}
Assumiamo che tale relazione sia invertibile, e quindi che esista un funzionale \( J_{\varphi} \) tale che
\begin{equation}
    \dfd{W}{J(x)}[J_{\varphi}] = \varphi(x)
\end{equation}
Definiamo allora
\begin{equation}
    \Gamma[\varphi] \coloneqq W[J_{\varphi}] - J_{\varphi} \cdot \varphi
\end{equation}
Osserviamo allora che
\begin{equation}
    \dfd{\Gamma}{\varphi(x)} = \int \dif[D]{y} \dfd{W}{J(y)}[J_{\varphi}] \dfd{J_{\varphi}(y)}{\varphi(x)} - \int \dif[D]{y} \dfd{J_{\varphi}(y)}{\varphi(x)} \varphi(y) - J_{\varphi}(x) = - J_{\varphi}(x)
\end{equation}
Usando che per definizione \( J_{\varphi_{J}} = J \) si ha
\begin{equation}
    \dfd{\Gamma}{\varphi(x)} [\varphi_{J}] = - J(x)
\end{equation}
Pertanto la relazione inversa tra \( \Gamma \) e \( W \) è
\begin{equation}
    W[J] = \Gamma[\varphi_{J}] + J \cdot \varphi_{J}
\end{equation}

Diamo ora un'interpretazione fisica a \( \Gamma \) mostrando che è il funzionale generatore dei diagrammi \emph{one-particle irreducible}. Consideriamo il seguente funzionale generatore in cui \( S \to \Gamma \) e dividiamo per un parametro \( g \) arbitrario
\begin{equation}
    \label{eq:azione-efficace-g}
    Z_{g, \Gamma}[J] = e^{i W_{g, \Gamma}[J]} = \int[\dif{\varphi}] \, e^{\frac{i}{g}\left(\Gamma[\varphi] + J\cdot\varphi\right)}
\end{equation}
Osserviamo che la presenza di \( g \) fa si che ogni vertice nell'espansione in diagrammi sia proporzionale a \( g \) e che ogni propagatore (inverso del termine quadratico in \( \Gamma \)) sia proporzionale a \( 1/g \). Se \( I \) è il numero di linee interne e \( V \) è il numero di vertici, il diagramma sarà quindi uguale a \( g^{V - I} \) per il diagramma che si ottiene dal funzionale con \( g = 1 \). In un diagramma connesso si ha che il numero di loop è \( L = V - I + 1 \) quindi il funzionale \( W_{g, \Gamma} \) avrà un'espansione in termini di numero di loops
\begin{equation}
    W_{g, \Gamma}[J] = \sum_{L=0}^{+\infty} g^{L - 1} W_{\Gamma}^{(L)}[J]
\end{equation}
dove \( W_{\Gamma}^{(L)}[J] \) è il contributo che si ottiene dai diagrammi con \( L \) loops nella teorica con \( S \to \Gamma \) e \( g = 1 \). Osserviamo ora che nel limite \( g \to 0 \) al LHS dell'equazione~\eqref{eq:azione-efficace-g} domina il termine a 0 loops, ovvero al \emph{tree level}
\begin{equation}
    Z_{g, \Gamma}[J] \simeq e^{\frac{i}{g} W_{\Gamma}^{(0)}[J]}
\end{equation}
Al RHS possiamo usare il metodo della fase stazionaria per cui
\begin{equation}
     \int[\dif{\varphi}] \, e^{\frac{i}{g}\left(\Gamma[\varphi] + J\cdot\varphi\right)} \simeq e^{\frac{i}{g}\left(\Gamma[\varphi_{J}] + J \cdot \varphi_{J}\right)}
\end{equation}
essendo \( \varphi_{J} \) il punto stazionario
\begin{equation}
    \left.\dfd{}{\varphi(x)} \left(\Gamma[\varphi] + J \cdot \varphi\right)\right\vert_{\varphi_{J}} = 0 \qquad \Rightarrow \qquad \dfd{\Gamma}{\varphi(x)}[\varphi_{J}] = - J(x)
\end{equation}
Pertanto
\begin{equation}
    iW_{\Gamma}^{(0)}[J] = \lim_{g \to 0} g \log Z_{g, \Gamma}[J] = i \left(\Gamma[\varphi_{J}] + J \cdot \varphi_{J}\right)
\end{equation}
Al RHS abbiamo ottenuto esattamente la trasformata di Legendre della \( \Gamma \), ovvero
\begin{equation}
    W_{\Gamma}^{(0)}[J] = W[J]
\end{equation}
cioé la \( W \) calcolata nella teoria con \( S \to \Gamma \) usando solo i diagrammi al tree-level è uguale al funzionale \( W \) della teoria con \( S \). Schematicamente scriviamo
\begin{equation}
    \label{eq:azione-efficace-tree-level}
    e^{iW[J]} = \int\limits_{\text{tree level}} [\dif{\varphi}] \, e^{i\Gamma[\varphi] + iJ \cdot \varphi}
\end{equation}
Per concludere basta osservare che una funzione di green connessa a \( n \) punti può sempre essere espressa in termini di diagrammi tree-level dove le linee sono sostituite da propagatori full mentre i vertici con \( n \) linee sono sostituiti dai diagrammi 1PI con \( n \) linee esterne. Affinché questa affermazione sia consistente con la~\eqref{eq:azione-efficace-tree-level} la \( \Gamma \) deve essere il funzionale generatore dei diagrammi 1PI. Più precisamente se scriviamo
\begin{equation}
    \Gamma[\varphi] = \sum_{n\geq 0} \frac{1}{n!} \int \Gamma^{(n)}(x_{1}, \ldots, x_{n}) \varphi(x_{1}) \cdots \varphi(x_{n})\dif[D]{x_{1}} \cdots \dif[D]{x_{n}}
\end{equation}
Deve essere
\begin{align}
  \Gamma^{(0)} &= G^{(0)} \\
  \Gamma^{(1)} &= \text{\textcolor{red}{boh, non ho ancora capito i tadpole}} \\
  \Gamma^{(2)} &= \frac{i}{G^{(2)}} \\
  \Gamma^{(n)} &= -i \sum (\text{diagrammi 1PI con \( n \) gambe esterne})
\end{align}

\subsection{Espansione in loop dell'azione efficace}
Consideriamo ora la teoria con \( g \neq 1 \) e azione \( S \)
\begin{equation}
    Z_{g}[J] = e^{i W_{g}[J]} = \int [\dif{\phi}] \, e^{\frac{i}{g}\left(S[\phi] + J \cdot \phi\right)}
\end{equation}
Analogamente a quanto detto per la \( W_{g, \Gamma} \), la \( W_{g} \) ammette un'espansione in loop
\begin{equation}
    W_{g}[J] = \sum_{L=0}^{+\infty} g^{L - 1} W^{(L)}[J]
\end{equation}
dove \(  W^{(L)}[J] \) è il contributo dei diagrammi connessi con \( L \) loops e \( g = 1 \). Definiamo \( \Gamma_{g} \) come trasformata di Legendre
\begin{equation}
    \Gamma_{g}[\varphi] = W_{g}[J_{g, \varphi}] - J_{g, \varphi} \cdot \varphi \qquad \dfd{W_{g}}{J(x)}[J_{g, \varphi}] = \varphi(x)
\end{equation}
Supponiamo che anche \( \Gamma_{g}, J_{g, \varphi}, \varphi_{g, J} \) abbiano un'espansione in loops(\textcolor{red}{queso non sono riuscito a mostrarlo formalmente})
\begin{equation}
    \Gamma_{g}[\varphi] = \sum_{L=0}^{+\infty} g^{L-1} \Gamma^{(L)}[\varphi] \qquad J_{g, \varphi} = \sum_{L=0}^{+\infty} g^{L-1} J_{\varphi}^{(L)} \qquad \varphi_{g, J} = \sum_{L=0}^{+\infty} g^{L-1} \varphi_{J}^{(L)}
\end{equation}

Facendo il limite \( g \to 0 \) sappiamo già che l'ordine leading è dato dall'approssimazione di fase stazionaria
\begin{equation}
    Z_{g}[J] \simeq e^{\frac{i}{g} W^{(0)}[J]} = e^{\frac{i}{g}\left(S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c}\right)} \qquad \Rightarrow \qquad W^{(0)}[J] = S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c}
\end{equation}
dove \( \phi_{J}\ap{c} \) è la soluzione di
\begin{equation}
    \dfd{S}{\phi(x)}[\phi_{J}\ap{c}] = - J(x)
\end{equation}
L'apice ``c'' è motivato dal fatto che per \( J = 0 \), \( \phi\ap{c} = \phi\ap{c}_{0} \) è il campo classico, cioé la configuazione di campo che stazionarizza l'azione
\begin{equation}
    \dfd{S}{\phi(x)}[\phi\ap{c}] = 0
\end{equation}

Si ha allora che
\begin{equation}
    \varphi_{g, J}(x) = \dfd{W_{g}}{J(x)} \simeq \frac{1}{g} \dfd{W^{(0)}}{J(x)}
    \qquad \Rightarrow \qquad
    \varphi_{J}^{(0)}(x) = \dfd{W^{(0)}}{J(x)} = \phi_{J}\ap{c}(x)
\end{equation}
Ma allora
\begin{equation}
    S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c} = W^{(0)}[J] = \Gamma^{(0)}[\varphi_{J}^{(0)}] + J \cdot \varphi_{J}^{(0)} = \Gamma^{(0)}[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c}
\end{equation}
da cui
\begin{equation}
    \Gamma^{(0)}[\phi_{J}\ap{c}] = S[\phi_{J}\ap{c}] \qquad \Rightarrow \qquad \Gamma^{(0)}[\varphi] = S[\varphi]
\end{equation}
se assumiamo che la mappa \( J \to \phi_{J}\ap{c} \) sia invertibile (e quindi suriettiva). \\

Per quanto riguarda il contributo a 1 loop, ricordiamo che se \( W \) dipende parametricamente da un parametro \( \lambda \), allora
\begin{equation}
    \dpd{W_{\lambda}}{\lambda}[J_{\lambda, \varphi}] + \dpd{\Gamma_{\lambda}}{\lambda}[\varphi] = 0
\end{equation}
Fattorizziamo la dipendenza da \( 1/g \) scrivendo
\begin{equation}
    W_{g}[J] = \frac{1}{g} W_{g}'[J] \qquad \Gamma_{g}[\varphi] = \frac{1}{g} \Gamma_{g}'[\varphi]
\end{equation}
\( \Gamma'_{g} \) e \( W'_{g} \) sono sempre legate da una trasformata di legendre si ha
\begin{equation}
    \dpd{W_{g}'}{g}[J] + \dpd{\Gamma_{g}'}{g}[\varphi_{g, J}'] = 0 \qquad \varphi'_{g, J} = g \varphi_{g, J}
\end{equation}
Ponendo \( g = 0 \) in tale espressione si ha
\begin{equation}
    W^{(1)}[J] + \Gamma^{(1)}[\varphi_{0, J}'] = 0
\end{equation}
\( \varphi_{0, J}' \) è semplicemente il campo calssico:
\begin{equation}
    \varphi_{g, J}' = \sum_{L=0}^{+\infty} g^{L} \varphi_{J}^{(L)} \qquad \Rightarrow \qquad \varphi_{0, J}' = \varphi_{J}^{(0)} = \phi_{J}\ap{c}
\end{equation}
quindi
\begin{equation}
    \Gamma^{(1)}[\phi_{J}\ap{c}] = - W^{(1)}[J]
\end{equation}

Per trovare la \( W^{(1)} \) facciamo un conto senza senso. Usando l'approssimazione di fase stazionaria andando al secondo ordine si ha
\begin{equation}
    S[\phi] + J \cdot \phi \simeq S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c} + \frac{1}{2} \int \dif[D]{x} \, \dif[D]{y} \left.\dfd{}{\phi(x)} \dfd{}{\phi(y)} S[\phi]\right\vert_{\phi_{J}\ap{c}} \left(\phi(x) - \phi_{J}\ap{c}(x)\right) \left(\phi(y) - \phi_{J}\ap{c}(y)\right)
\end{equation}
Pertanto
\begin{align}
  Z_{g}[J] &\simeq \int [\dif{\phi}] \, e^{\frac{i}{g}\left(S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c} + \frac{1}{2} (\phi - \phi_{J}\ap{c}) K_{S}  (\phi - \phi_{J}\ap{c})\right)} \\
  &= e^{\frac{i}{g}\left(S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c}\right)} \left[\det\left(\frac{-i K_{S}}{2\pi g}\right)\right]^{-1/2}
\end{align}
dove
\begin{equation}
    K_{S}(x, y) = \left.\dfd{}{\phi(x)} \dfd{}{\phi(y)} S[\phi]\right\vert_{\phi_{J}\ap{c}}
\end{equation}
Usando che \( \tr \log = \log \det \) si ha
\begin{equation}
    iW_{g}[J] \simeq \frac{i}{g}\left(S[\phi_{J}\ap{c}] + J \cdot \phi_{J}\ap{c}\right) - \frac{1}{2} \tr \log (K_{S}) + \frac{1}{2} \tr \log(2\pi i g)
\end{equation}
\textcolor{red}{A quanto pare si ignora completamente il pezzo il \( \log g \)} per cui infine
\begin{equation}
    W^{(1)}[J] = \frac{i}{2} \tr \log \left[\left.\dfd{}{\phi(x)} \dfd{}{\phi(y)} S[\phi]\right\vert_{\phi_{J}\ap{c}}\right]
\end{equation}
L'azione efficace a 1 loop è quindi
\begin{equation}
    \Gamma^{(1)}[\varphi] = - \frac{i}{2} \tr \log \left[\left.\dfd{}{\phi(x)} \dfd{}{\phi(y)} S[\phi]\right\vert_{\varphi}\right]
\end{equation}
Nel caso il cui
\begin{equation}
    S[\phi] = \frac{1}{2} \phi K \phi + \lambda \int \dif[D]{x} V(\phi(x))
\end{equation}
si ha
\begin{equation}
    \dfd{}{\phi(x)} \dfd{}{\phi(y)} S[\phi] = K(x, y) + \lambda V''(\phi(x)) \delta^{(D)}(x - y)
\end{equation}
Pertanto
\begin{equation}
    \Gamma^{(1)}[\varphi] = - \frac{i}{2} \tr \log K - \frac{i}{2} \tr \log \left[1 + \lambda K^{-1} V''(\varphi)\right]
\end{equation}
per cui, essendo il propagatore libero \( \Delta = i K^{-1} \) e espandendo in serie di potenze della costante di accoppiamento
\begin{equation}
    \Gamma^{(1)}[\varphi] = - \frac{i}{2} \tr \log K - \frac{i}{2} \sum_{n\geq 0} \frac{(i \lambda)^{n}}{n} \int \dif[D]{x_{1}} \cdots \dif[D]{x_{n}} V''(\varphi(x_{1})) \Delta(x_{1}, x_{2})  V''(\varphi(x_{x})) \cdots V''(\varphi(x_{n})) \Delta(x_{n}, x_{1})
\end{equation}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
