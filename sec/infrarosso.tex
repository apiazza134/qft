\section{Effetti infrarossi}

\subsection{Ampiezze con fotoni soffici}
Consideriamo un processo \( a \to b \) e sia \( \mathcal{M}_{a \to b} \) l'ampiezza invariante associata. Consideriamo ora il diagramma \( a \to b + \gamma \) in cui aggiungiamo un fotone di impulso \( q \) e polarizzazione \( \lambda \) ad una linea uscente \( b_{i} \). Vediamo alcuni casi e poi generalizziamo
\begin{itemize}
    \item se \( b_{i} \) è una particella massiva di spin 0 (carica) l'interazione è
    \begin{equation}
        \mathscr{L}_{\phi\phi A} = i e A_{\mu}\left(\phi\partial^{\mu}\phi^{*} - \phi^{*} \partial^{\mu}\phi\right) \qquad \Rightarrow \qquad
        \feynmandiagram [inline=(b.base), horizontal=b to d, scale=0.9] {
            a -- [charged scalar, momentum'=\(p\)] b -- [charged scalar, momentum=\(p'\)] c,
            b -- [photon, momentum=\(q\)] d,
        };
        \sim i e (p_{\mu}' + p_{\mu})
    \end{equation}
    Pertanto l'ampiezza diventa
    \begin{align}
      \mathcal{M}_{a \to b + \gamma} &= \varepsilon^{\mu}(q, \lambda) [i e_{i} (p_{\mu}^{i} + q_{\mu} + p_{\mu}^{i}) ] \frac{i}{(p_{i} + q)^{2} - m^{2} + i \epsilon} \mathcal{M}_{a \to b}(p_{i} \to p_{i} + q) \\
                                     &= - e_{i} \varepsilon^{\mu}(q, \lambda) \frac{2p^{i}_{\mu} + q_{\mu}}{2 p_{i} \cdot q + q^{2} + i \epsilon} \mathcal{M}_{a \to b}(p_{i} \to p_{i} + q)
    \end{align}
    Nel limite \( q \to 0 \) si ha allora
    \begin{equation}
        \mathcal{M}_{a \to b + \gamma} \simeq - e_{i} \varepsilon^{\mu}(0, \lambda) \frac{p^{i}_{\mu}}{p_{i} \cdot q + i \epsilon} \mathcal{M}_{a \to b}
    \end{equation}

    \item se \( b_{i} \) è una particella massiva di spin 1/2 (carica) l'interazione è
    \begin{equation}
        \mathscr{L}_{\phi\phi A} = i e A_{\mu} \bar{\psi} \gamma^{\mu} \psi \qquad \Rightarrow \qquad
        \feynmandiagram [inline=(b.base), horizontal=b to d, scale=0.9] {
            a -- [fermion, momentum'=\(p\)] b -- [fermion, momentum=\(p'\)] c,
            b -- [photon, momentum=\(q\)] d,
        };
        \sim i e \gamma_{\mu}
    \end{equation}
    Pertanto l'ampiezza diventa
    \begin{align}
      \mathcal{M}_{a \to b + \gamma} &= \varepsilon^{\mu}(q, \lambda) \bar{u}(p_{i} + q, \sigma) [i e_{i} \gamma_{\mu} ] \frac{i}{\slashed{p}_{i} + \slashed{q} - m  + i \epsilon} \mathcal{M}_{a \to b}'(p_{i} \to p_{i} + q) \\
                                     &=  - e_{i}\varepsilon^{\mu}(q, \lambda) \bar{u}(p_{i} + q, \sigma) \gamma_{\mu} \frac{\slashed{p}_{i} + \slashed{q} + m}{(p_{i} + q)^{2} - m^{2}  + i \epsilon} \mathcal{M}_{a \to b}'(p_{i} \to p_{i} + q)
    \end{align}
    Dove \( \mathcal{M}' \) è l'ampiezza privata del vettore di polarizzazione esterno. Nel limite \( q \to 0 \) si ha allora
    \begin{equation}
        \mathcal{M}_{a \to b + \gamma} \simeq - e_{i} \varepsilon^{\mu}(0, \lambda) \bar{u}(p_{i}, \sigma)\gamma_{\mu} \frac{\slashed{p}_{i} + m}{2 p_{i} \cdot q + i \epsilon} \mathcal{M}_{a \to b}
    \end{equation}
    Ora
    \begin{align}
      \bar{u}(p, \sigma)\gamma_{\mu} (\slashed{p} + m) &= \bar{u}(p, \sigma)\gamma_{\mu} \sum_{\sigma'=1, 2} u(p, \sigma') \bar u(p, \sigma') = \sum_{\sigma'=1, 2} [\bar{u}(p, \sigma)\gamma_{\mu} u(p, \sigma')] \bar u(p, \sigma') \\
      &= 2 p_{\mu} \sum_{\sigma'=1, 2} \bar u(p, \sigma') \delta_{\sigma \sigma'} = 2 p_{\mu} \bar u(p, \sigma)
    \end{align}
    Pertanto
    \begin{equation}
        \mathcal{M}_{a \to b + \gamma}  \simeq - e_{i} \varepsilon^{\mu}(0, \lambda) \frac{p^{i}_{\mu}}{p_{i} \cdot q + i \epsilon} \mathcal{M}_{a \to b}
    \end{equation}

    \item Pare le la struttura sia generale ma la giustificazione è molto criptica
\end{itemize}

Consideriamo adesso l'intero processo \( a \to b + \gamma \): il fotone può essere attaccato a una linea entrante, a una linea uscente o a una linea interna. Se è attaccato ad una linea interna darà un contributo subleading rispetto a quelli precententi in quanto i propagatori interni non sono on-shell e quindi sono meno singolari di \( 1 / p \cdot q \) nel limite \( q \to 0 \). Pertanto
\begin{equation}
    \mathcal{M}_{a \to b + \gamma} \simeq \mathcal{M}_{a \to b} \varepsilon^{\mu}(0, \lambda)\sum_{i} \eta_{i} e_{i} \frac{p_{\mu}^{i}}{p_{i} \cdot q}
\end{equation}
dove \( \eta_{i} = 1 \) e \( i \) labella una linea entrante, e \( \eta_{i} = -1 \) se \( i \) labella una linea uscente e \( e_{i} \) sono le cariche delle particelle entranti e uscenti.

Anche nel processo \( a \to b + 2 \gamma \) abbiamo diversi casi: se i 2 fotoni vengono da due linee diverse abbiamo dei fattori moltiplicativi uguali a quelli di prima; se provengono dalla stessa linea dobbiamo tenere conto della simmetria per scambio dei fotoni e quindi abbiamo un contributo
\begin{align}
  \mathcal{M}_{a \to b + 2\gamma} &\supset \left[\frac{\eta_{i} e_{i} p_{\mu}^{i}}{p_{i} \cdot q_{1}} \cdot \frac{\eta_{i} e_{i} p_{\nu}^{i}}{p_{i} \cdot (q_{1} + q_{2})} + \frac{\eta_{i} e_{i} p_{\nu}^{i}}{p_{i} \cdot q_{2}} \cdot \frac{\eta_{i} e_{i} p_{\mu}^{i}}{p_{i} \cdot (q_{2} + q_{1})} \right] \varepsilon^{\mu}(0, \lambda_{1}) \varepsilon^{\nu}(0, \lambda_{2}) \mathcal{M}_{a \to b} \\
  &= \left[\frac{\eta_{i} e_{i} p_{\mu}^{i}}{p_{i} \cdot q_{1}}\right] \cdot \left[\frac{\eta_{i} e_{i} p_{\nu}^{i}}{p_{i} \cdot q_{2}}\right] \varepsilon^{\mu}(0, \lambda_{1}) \varepsilon^{\nu}(0, \lambda_{2}) \mathcal{M}_{a \to b}
\end{align}
quindi il contributo si fattorizza. Procedendo per induzione si trova che il contributo dominante al processo \( a \to b + r \cdot \gamma \) nel limite \( q \to 0 \) è
\begin{equation}
    \mathcal{M}_{a \to b + r\gamma} \simeq \mathcal{M}_{a \to b} \prod_{l=1}^{r} \left(\sum_{i} \frac{\eta_{i} e_{i}  p_{\mu_{l}}^{i}}{p_{i} \cdot q_{l}}\right) \epsilon^{\mu_{l}}(0, \lambda_{l})
\end{equation}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
