\section{Path integral}
Consideriamo un sistema quantistico in cui siano definiti operatori posizione \( \{Q_{a}\}_{a \in I} \) e impulso coniugato \( \{P_{b}\}_{a \in I} \) che soddisfino le usuali regole di commutazione canonica
\begin{equation}
    [Q_{a}, P_{b}] = i \delta_{ab} \qquad [Q_{a}, Q_{b}] = [P_{a}, P_{b}] = 0
\end{equation}
Nel caso di una teoria di campo l'indice \( a \) è in realtà una coppia di un indice continuo ``spaziale'' \( \vec{x} \) e di un set di indici discreti che indichiamo globalmente con ``m'' che raggruppano il tipo di specie e indici discreti ``di Lorentz''. Di solito scriviamo quindi
\begin{equation}
    Q_{a} \to Q_{m}(\vec{x}) \qquad P_{a} \to P_{m}(\vec{x}) \qquad \delta_{ab} \to \delta^{(3)}(\vec{x} - \vec{x}') \delta_{m m'}
\end{equation}
Quando la cosa non è ambigua usiamo la notazione con l'indice latino che è più compatta. Possiamo diagonalizzare separatamente i \( \{Q_{a}\} \) e i \( \{P_{a}\} \) trovando un set completo di autostati ``impropri'' con autovalori continui
\begin{gather}
    Q_{a}\ket{q} = q_{a}\ket{q} \qquad \braket{q' | q} = \prod_{a} \delta(q'_{a} -  q_{a}) = \delta(q' - q) \\
    P_{a}\ket{p} = p_{a}\ket{p} \qquad \braket{p' | p} = \prod_{a} \delta(p'_{a} -  p_{a}) = \delta(p' - p)
\end{gather}
dove intendiamo \( q = (q_{a})_{a \in I} \) e \( p = (p_{a})_{a \in I} \). Usando semplicemente le regole di commutazione si ottiene
\begin{equation}
    \braket{q | p} = \prod_{a} \frac{1}{\sqrt{2\pi}} e^{i q_{a} p_{a}}
\end{equation}

Gli operatori in rappresentazione di Heisenberg sono
\begin{equation}
    Q_{a}(t) = e^{i H t} Q_{a} e^{-i H t} \qquad P_{a}(t) = e^{i H t} P_{a} e^{-i H t}
\end{equation}
Gli stati \( \ket{q, t} = e^{iHt} \ket{q} \) e \( \ket{p, t} = e^{iHt} \ket{p} \) sono autostati istantanei di impulso e posizione con autovalore \( q_{a} \) e \( p_{a} \)
\begin{equation}
    Q_{a}(t) \ket{q, t} = q_{a} \ket{q, t} \qquad P_{a}(t) \ket{p, t} = p_{a} \ket{p, t}
\end{equation}
che soddisfano le stesse normalizzazione degli stati ``non evoluti''
\begin{equation}
    \braket{q', t | q, t} = \delta(q' - q) \qquad \braket{p', t | p, t} = \delta(p' - p) \qquad \braket{q, t | p, t} = \prod_{a} \frac{1}{\sqrt{2\pi}} e^{iq_{a}p_{a}}
\end{equation}
Se il sistema si trova in \( q \) al tempo \( t \), l'ampiezza di probabilità per trovarlo al tempo \( t' \) in \( q' \) è
\begin{equation}
    \braket{q', t' | q, t} = \braket{q', t | e^{-i H(t' - t)} | q, t}
\end{equation}
L'hamiltoniana \( H \) è funzione di \( P_{a} \) e \( Q_{a} \) ma dato che commuta con se stessa è anche \( H(Q(t), P(t)) = H(Q, P) \)
Se \( t' = t + \Delta \) per \( \Delta \to 0 \) si ha
\begin{equation}
    \braket{q', t + \Delta |  q, t} = \braket{q', t | e^{-i H \Delta} | q, t}  = \int \prod_{a}\dif{p_{a}} \braket{q', t | \exp\left(i H(Q(t), P(t)) \Delta\right) | p, t} \braket{p, t | q, t}
\end{equation}
Supponendo ora che \( H(Q_{a}, P_{a}) \) sia un polinomio dei campi scritto in modo che le \( Q_{a} \) appaiano sempre a \emph{sinistra} dei \( P_{a} \). In questo modo
\begin{equation}
    \braket{q, t | H(Q(t), P(t))| p, t} = \braket{q | H(Q, P) | p} = H(q, p)
\end{equation}
Così
\begin{equation}
    \braket{q', t + \Delta |  q, t} = \int \prod_{a} \frac{\dif{p}}{2\pi} \exp\left(-i H(q', p) \Delta + i \sum_{a}(q'_{a} - q_{a}) p_{a}\right)
\end{equation}
Se separiamo ora l'intervallo \( [t, t'] \) in \( N + 1\) intervalli di lunghezza \( \Delta = (t' - t) / (N + 1) \) e posto \( \tau_{k} = t + k \Delta \) per \( k = 1, \ldots N \) possiamo scrivere
\begin{align*}
  \braket{q', t' | q, t} &= \int \dif{q_{1}} \cdots \dif{q_{N}} \braket{q', t' | q_{N}, \tau_{N}} \cdots \braket{q_{2}, \tau_{2} | q_{1}, \tau_{1}} \braket{q_{1}, \tau_{1} | q, t} \\
                         &= \int \left[\prod_{k=1}^{N} \prod_{a} \dif{q_{k,a}}\right] \left[\prod_{k=0}^{N} \prod_{a} \frac{\dif{p_{k, a}}}{2\pi}\right] \exp\left\{i \sum_{k=1}^{N+1}\left[\sum_{a}(q_{k,a} - q_{k-1, a}) p_{k-1, a} - H(q_{k}, p_{k-1}) \Delta\right]\right\}
\end{align*}
dove si intende \( q_{0} = q \) e \( q_{N+1} = q' \). Se ora consideriamo delle funzioni \( q(\tau) \) e \( p(\tau) \) che interpolino i valori \( q_{k} \) e \( p_{k} \) in \( \tau_{k} \), ovvero
\begin{equation}
    q_{a}(\tau_{k}) = q_{k,a} \qquad p_{a}(\tau_{k}) = p_{k, a}
\end{equation}
Per \( \Delta \to 0 \) la sommatoria a esponente diventa un integrale
\begin{equation}
    \sum_{k=1}^{N+1}\left[(q_{k,a} - q_{k-1, a}) p_{k-1, a} - H(q_{k}, p_{k-1}) \Delta\right] \to \int_{t}^{t'}\left[\sum_{a}\dot{q}_{a}(\tau) p_{a}(\tau) - H(q(\tau), p(\tau)) \right] \dif{\tau}
\end{equation}
Il limite della misura definisce è scritto in modo ``formale'' come un integrale sullo spazio di tutte le possibili funzioni \( q(\tau) \) e \( p(\tau) \) con \( q(t) = q \) e \( q(t') = q' \)
\begin{align*}
  \braket{q', t' | q, t} &= \int\limits_{\substack{q(t) = q \\  q(t') = q'}} \frac{[\dif q(\tau)] [\dif p(\tau)]}{2\pi} \exp\left\{i\int_{t}^{t'}\left[\sum_{a} \dot{q}_{a}(\tau) p_{a}(\tau) - H(q(\tau), p(\tau)) \right] \dif{\tau}\right\} \\
                         &= \int\limits_{\substack{q(t) = q \\  q(t') = q'}} \frac{[\dif q(\tau)] [\dif p(\tau)]}{2\pi} \, e^{i S_{H}[q(\tau), p(\tau)]}
\end{align*}
ove
\[
    [d q(\tau)] \leftarrow  \prod_{k} \prod_{a} \frac{\dif{q_{k,a}}}{\sqrt{2\pi}} \qquad
    [d p(\tau)] \leftarrow  \prod_{k} \prod_{a} \frac{\dif{p_{k,a}}}{\sqrt{2\pi}}
\]

Analogamente se \( O(t) = O[Q(t), P(t)] \) è un polinomio di operatori sempre con la convenzione di avere le \( Q \) a \emph{sinistra} delle \( P \) si ottiene
\begin{equation}
    \braket{q', t' | \T\{O_{1}(t_{1}) \cdots O_{n}(t_{n})\} |  q, t} = \int\limits_{\substack{q(t) = q \\  q(t') = q'}} \frac{[\dif q(\tau)] [\dif p(\tau)]}{2\pi} \, O_{1}[q(t_{1}), p(t_{1})] \cdots O_{n}[q(t_{n}), p(t_{n})] \, e^{i S_{H}[q(\tau), p(\tau)]}
\end{equation}
Le variabili \( q(\tau) \) e \( p(\tau) \) non soddisfano le equazioni del moto, ma vale comunque
\begin{equation}
    \frac{\delta S_{H}[q, p]}{\delta q_{a}(t)} = - \dot{p}_{a}(t) - \dpd{H(q(t), p(t))}{q_{a}} \qquad
    \frac{\delta S_{H}[q, p]}{\delta p_{a}(t)} = \dot{q}_{a}(t) - \dpd{H(q(t), p(t))}{p_{a}}
\end{equation}

Ricordiamo che se che se \( Q(x) \) è una forma quadratica
\begin{equation}
    Q(x) = \frac{1}{2} \sum_{a, b} x_{a} A_{ab} x_{b} + \sum_{b} B_{a} x_{a} + C
\end{equation}
allora
\begin{equation}
    \int \prod_{a} \frac{\dif{x_{a}}}{\sqrt{2\pi}} e^{-Q(x)} = \frac{1}{\sqrt{\det A}} e^{-Q(\bar{x})}
\end{equation}
essendo \( \bar{x} \) il punto stazionario della forma quadratica
\begin{equation}
    \dpd{Q}{x_{a}}(\bar{x}) = 0 \qquad \forall a
\end{equation}

Pertanto se supponiamo che l'hamiltoniana sia una forma quadratica negli impulsi
\begin{equation}
    H(q, p) = \frac{1}{2} \sum_{a, b} p_{a} A_{ab}(q) p_{b} + \sum_{a} B_{a}(q) p_{a} + V(q)
\end{equation}
allora possiamo svolgere l'integrale in \( [\dif p] \) e ottenere
\begin{equation}
    \braket{q', t' | q, t} = \int\limits_{\substack{q(t) = q \\  q(t') = q'}} \frac{[\dif q(\tau)]}{2\pi} \frac{1}{\sqrt{\det A[q(\tau)]}} \, e^{i S_{H}[q(\tau), \bar{p}(\tau)]}
\end{equation}
essendo \( \bar{p}_{a} \) l'impulso ``classico''

\begin{equation}
    0 = \frac{\delta S_{H}[q, \bar{p}]}{\delta p_{a}(t)} = \dot{q}_{a}(t) - \left. \dpd{H(q(t), p(t))}{p_{a}} \right|_{p_{a} = \bar{p}_{a}}
\end{equation}
L'azione \( S_{H} \) calcolata in \( p_{a} = \bar{p}_{a} \) è l'integrale della trasformata di Legendre della lagrangiana
\begin{equation}
    S_{H}[q, \bar{p}] = \int \mathcal{L}(q(\tau), \dot{q}(\tau)) \dif{\tau} = S[q]
\end{equation}
Così infine
\begin{equation}
    \braket{q', t' | q, t} = \int\limits_{\substack{q(t) = q \\  q(t') = q'}} [\dif q(\tau)] \frac{1}{\sqrt{\det(2\pi A[q(\tau)])}} \, e^{i S[q(\tau)]}
\end{equation}
dove abbiamo assorbito spostato il fattore \( 1/\sqrt{2\pi} \) della misura nel determinante.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
