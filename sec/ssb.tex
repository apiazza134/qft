\section{Rottura spontanea di simmetria}

\subsection{Rottura spontanea in teorie di gauge}
Consideriamo un campo scalare reale \( \Phi \) che trasforma un una rappresentazione lineare unitaria \( R \) di un gruppo \( G \): \( \Phi \to R \Phi \). Siano \( \Phi = (\phi_{n}) \) le componenti del campo
\begin{equation}
    \phi_{n} \xrightarrow{G} \phi'_{n} = R_{nm} \phi_{m}
\end{equation}

Consideriamo una teoria di gauge con gruppo \( G \) e sia \( A_{\mu} = A_{\mu}^{a} t^{a} \) il campo di gauge e \( t^{a} \) i generatori di \( G \) nella rappresentazione \( R \). La trasformazione di guauge è
\begin{equation}
    A_{\mu} \to A_{\mu}' = R A_{\mu} R^{\dagger} - \frac{i}{g} R^{\dagger} \partial_{\mu} R
\end{equation}
e la derivata covariante è
\begin{equation}
    D_{\mu} \Phi = \partial_{\mu} \Phi + i g A_{\mu} \Phi
    \qquad \rightarrow \qquad
    D_{\mu} \phi_{n} = \partial_{\mu} \phi_{n} + i g A_{\mu}^{a} t^{a}_{nm} \phi_{m}
\end{equation}

La lagrangiana all'ordine più basso in impulsi è
\begin{equation}
    \mathscr{L} = -\frac{1}{2} \tr\left[F^{\mu\nu} F_{\mu\nu}\right] + \frac{1}{2} (D^{\mu} \Phi)^{\dagger} D_{\mu} \Phi - V(\Phi^{\dagger} \Phi)
\end{equation}
Supponiamo ora che il gruppo \( G \) sia spontaneamente rotto a un sottogruppo \( H \) dal valore di aspettazione del campo \( \Phi \) sul vuoto
\begin{equation}
    \braket{\phi_{n}(x)} = v_{n}
\end{equation}
Riscriviamo il campo come \( \Phi(x) = v + \tilde{\Phi}(x) \). Il termine cinetico dà un contributo alla lagrangiana quadratica
\begin{align}
  \mathscr{L}_{2} &\supset \frac{1}{2}\left(\partial_{\mu} \tilde{\Phi}^{\dagger} - i g v^{\dagger} A_{\mu}\right)\left(\partial^{\mu}\tilde{\Phi} +  i g A^{\mu} v\right) \\
  &= \frac{1}{2}\partial_{\mu} \tilde{\Phi}^{\dagger} \partial^{\mu} \tilde{\Phi} + \frac{i g}{2} A_{\mu}^{a} \left(\partial^{\mu}\tilde{\Phi}^{\dagger} t^{a} v - v^{\dagger} t^{a} \partial^{\mu}\tilde{\Phi}\right) + \frac{g^{2}}{2} v^{\dagger}t^{a} t^{b} v \, A_{\mu}^{a} A^{b\mu}
\end{align}
Per quantizzare la teoria dobbiamo introdurre un gauge fixing e i ghosts. Scegliamo un termine di guauge fixing per cancellare il mixing \( \tilde{\Phi}-A_{\mu} \)
\begin{align}
  \mathscr{L}\ped{GF} &= - \frac{1}{2\xi} \left(\partial^{\mu}A_{\mu}^{a} + i \xi g v^{\dagger} t^{a} \tilde{\Phi}\right) \left(\partial^{\mu}A_{\mu}^{a} - i \xi g  \tilde{\Phi}^{\dagger} t^{a} v\right) \\
  &= - \frac{1}{2\xi} (\partial^{\mu}A_{\mu}^{a})^{2} + \frac{ig}{2} \partial^{\mu}A_{\mu}^{a} \left(\tilde{\Phi}^{\dagger} t^{a} v  - v^{\dagger} t^{a} \tilde{\Phi} \right) - \frac{\xi g^{2}}{2} v^{\dagger} t^{a} \tilde{\Phi} \tilde{\Phi}^{\dagger} t^{a} v
\end{align}
Il termine misto si cancella una volta integrato per parti
\begin{equation}
    \mathscr{L}_{2} + \mathscr{L}\ped{GF} \supset -\frac{1}{2} \tr\left[F^{\mu\nu} F_{\mu\nu}\right] - \frac{1}{2\xi} (\partial^{\mu}A_{\mu}^{a})^{2} + \frac{g^{2}}{2} v^{\dagger}t^{a} t^{b} v \, A_{\mu}^{a} A^{b\mu} + \frac{1}{2}\partial_{\mu} \tilde{\Phi}^{\dagger} \partial^{\mu} \tilde{\Phi} - \frac{\xi g^{2}}{2} v^{\dagger} t^{a} \tilde{\Phi} \tilde{\Phi}^{\dagger} t^{a} v
\end{equation}
La lagrangiana quadratica per il campo di gauge è
\begin{equation}
    \mathscr{L}_{2A} = - \frac{1}{2} \tr\left[F^{\mu\nu} F_{\mu\nu}\right] - \frac{1}{2\xi} (\partial^{\mu}A_{\mu}^{a})^{2} + \frac{1}{2} \mathfrak{m}_{ab}^{2}  \, A_{\mu}^{a} A^{b}_{\mu}
\end{equation}
dove abbiamo introdotto la matrice di massa quadra
\begin{equation}
    \mathfrak{m}_{ab}^{2} = g^{2} v^{\dagger}t^{a} t^{b} v
\end{equation}
La lagrangiana è ancora invariante sotto trasformazioni globali del sottogruppo non rotto \( H \), infatti possiamo riscriverla come
\begin{equation}
    \mathscr{L}_{2A} = - \frac{1}{2} \tr\left[F^{\mu\nu} F_{\mu\nu} + \frac{2}{\xi} \partial_{\mu}A^{\mu} \partial_{\nu} A^{\nu}\right] + \frac{1}{2} v^{\dagger} A_{\mu} A^{\mu} v
\end{equation}
dove il termine con la traccia è manifestamente invariante, mentre il termine di massa è invariante essendo il vev invariante sotto l'azione del gruppo residuo.

La funzione di guauge fixing che abbiamo introdotto è
\begin{equation}
    G^{a}_{\theta}(x) = \partial^{\mu}{A_{\mu}^{a}}^{(\theta)}(x) + i \xi g  v^{\dagger} t^{a} \tilde{\Phi}^{(\theta)}(x)
\end{equation}
Per una trasformazione di guauge infinitesima \( R = e^{ig \Theta} \simeq 1 + i g \Theta \) si ha
\begin{equation}
    \delta A_{\mu} = i g [\Theta, A_{\mu}] + \partial_{\mu} \Theta \qquad \delta\tilde{\Phi} = i g \Theta v  + i g \Theta \tilde{\Phi}
\end{equation}
per cui, posto \( \Theta(x) = \theta^{a}(x) t^{a} \),
\begin{equation}
    \delta A_{\mu}^{a} = - g f^{abc} \theta^{b} A_{\mu}^{c} + \partial_{\mu} \theta^{a}
\end{equation}
La variazione di \( G^{a}_{\theta} \) è quindi
\begin{equation}
    \delta G^{a}_{\theta} = - g f^{abc}\partial^{\mu} \theta^{b}  A_{\mu}^{c} - g f^{abc} \theta^{b} \partial^{\mu} A_{\mu}^{c} + \Box \theta^{a} - \xi g^{2} \left(v^{\dagger} t^{a} t^{b} v \right) \theta^{b} - \xi g^{2} \left(v^{\dagger} t^{a} t^{b} \tilde{\Phi} \right) \theta^{b}
\end{equation}
Pertanto la matrice di Faddeev-Popov è
\begin{equation}
    M^{ab}(x, y) = \left[\delta^{ab} \Box_{x} - g f^{abc} A_{\mu}^{c} \partial^{\mu}_{x} - g f^{abc} \partial^{\mu} A_{\mu}^{c} - \xi g^{2} \left(v^{\dagger} t^{a} t^{b} v \right)  - \xi g^{2} \left(v^{\dagger} t^{a} t^{b} \tilde{\Phi} \right) \right]  \delta^{(4)}(x - y)
\end{equation}
L'azione dei ghosts continente quindi un termine quadratico
\begin{align}
  S\ped{ghost} &= - \int \dif^{4}{x} \dif^{4}{y} \, \bar{c}^{a} M^{ab}(x, y) c^{b}(y) \\
  &\supset \int \dif^{4}{x} \, \left[-\bar{c}^{a} \Box c^{a} + \xi g^{2}  \left(v^{\dagger} t^{a} t^{b} v \right) \bar{c}^{a} c^{b}\right]
\end{align}
La lagrangiana quadratica completa è quindi
\begin{align}
  \mathscr{L}_{2} &= - \frac{1}{2} \tr\left[F^{\mu\nu} F_{\mu\nu}\right] - \frac{1}{2\xi} (\partial^{\mu}A_{\mu}^{a})^{2} + \frac{1}{2} \mathfrak{m}_{ab}^{2}  \, A_{\mu}^{a} A^{b}_{\mu} \\
                  &\quad + \frac{1}{2}\partial_{\mu}\tilde{\phi}_{n} \partial^{\mu} \tilde{\phi}_{n} - \frac{1}{2} M_{nm}^{2} \tilde{\phi}_{n}\tilde{\phi}_{m} \\
                  &\quad + \partial_{\mu} \bar{c}^{a} \partial^{\mu} c^{a} + \xi \mathfrak{m}_{ab}^{2} \bar{c}^{a} c^{b}
\end{align}
dove la matrice di massa del campo scalare è data da
\begin{equation}
    M^{2}_{nm} = \mu_{nm}^{2} + \xi g^{2} (v^{\dagger} t^{a})_{n} (t^{a} v)_{m} \qquad \mu_{nm}^{2} = \left.\dmd{V}{2}{\phi_{n}}{}{\phi_{m}}{}\right\vert_{\phi_{k} = v_{k}}
\end{equation}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
