\section{Campi quantistici}
Affinché la matrice \( \S \) sia invariante di Lorentz l'interazione deve essere l'integrale spaziale di una densità hamiltoniana
\begin{equation}
    V(t) = \int \mathscr{V}(t, \vec{x}) \dif[3] {\vec{x}}
\end{equation}
che sia scalare sotto trasformazione di Lorentz, ovvero che soddisfi
\begin{equation}
    \label{}
    U_{0}(\Lambda, a) \mathscr{V}(x) U^{-1}_{0}(\Lambda, a) = \mathscr{V}(\Lambda x + a)
\end{equation}
e che sia ``causale'', ovvero che
\begin{equation}
    \label{}
    [\mathscr{V}(x), \mathscr{V}(x')] = 0 \qquad \text{per} \quad {(x - x')}^{2} < 0
\end{equation}

Per soddisfare il principio di decomposizione in cluster possiamo costruire la densità hamiltoniana a partire da operatori di creazione e distruzione. Tuttavia questi trasformano in modo non banale sotto trasformazioni di Lorentz e in particolare si beccano degli elementi di matrice che dipendono dall'impulso associato all'operatore in questione. Una soluzione è quella di costruire l'hamiltoniana a partire da \emph{campi} che vivano all'interno di rappresentazioni irriducibili \emph{finito dimensionali} del gruppo di Lorentz.

Per ora lavoriamo con particelle massive, visto che per quelle non massive la notazione in realtà è più semplice. Indicando con \( a(\vec{p}, \sigma, n) \) l'operatore di distruzione di singola particella, introduciamo i capi di creazione e distruzione
\begin{equation}
    \hat{\psi}^{+}_{l}(x) = \sum_{\sigma, n} \int \Dp \, u_{l}(x, \vec{p}, \sigma, n) a(\vec{p}, \sigma, n)
    \qquad
    \hat{\psi}^{-}_{l}(x) = \sum_{\sigma, n} \int \Dp \, v_{l}(x, \vec{p}, \sigma, n) a^{\dagger}(\vec{p}, \sigma, n)
\end{equation}
dove \( l \) è un indice discreto e i coefficienti \( u_{l} \) e \( v_{l} \) sono scelti in modo che sotto trasformazione di Lorentz
\begin{align}
  U_{0}(\Lambda, a) \psi_{l}^{+}(x) U_{0}^{-1}(\Lambda, a) = \sum_{l'} D_{l l'}^{+}(\Lambda^{-1}) \psi_{l'}^{+}(\Lambda x + a)  \\
  U_{0}(\Lambda, a) \psi_{l}^{-}(x) U_{0}^{-1}(\Lambda, a) = \sum_{l'} D_{l l'}^{-}(\Lambda^{-1}) \psi_{l'}^{-}(\Lambda x + a)
\end{align}
Si vede per definizione che le matrici \( D^{\pm} \) sono una rappresentazione finito dimensionale del gruppo di Lorentz. In generale tale rappresentazione sarà riducibile e quindi potremmo ridefinire i campi e l'indice \( l \) in modo che \( D^{\pm} \) sia blocco-diagonale.

A questo punto possiamo costruire un'interazione come polinomio dei campi con interazioni locali
\begin{equation}
    \label{eq:interazione}
    \hat{\mathscr{V}}(x) = \sum_{n, m} \sum_{h_{j}}\sum_{l_{i}}  g_{h_{1} \cdots h_{n} l_{1} \cdots l_{m}}(x) \hat{\psi}_{h_{1}}^{-}(x) \cdots \hat{\psi}_{h_{n}}^{-}(x) \hat{\psi}_{l_{1}}^{+}(x) \cdots \hat{\psi}_{l_{m}}^{+}(x)
\end{equation}
con il \( c \)-numero \( g \) scelto in modo che \( \mathscr{V} \) sia scalare, ovvero che soddisfi
\begin{equation}
    \sum_{h_{i}} \sum_{l_{j}} g_{h_{1} \cdots h_{n} l_{1} \cdots l_{m}}(x) D_{h_{1} h_{1}'}^{-}(\Lambda^{-1}) \cdots D_{h_{n} h_{n}'}^{-}(\Lambda^{-1}) D_{l_{1} l_{1}'}^{+}(\Lambda^{-1}) \cdots D_{l_{m} l_{m}'}^{+}(\Lambda^{-1}) = g_{h'_{1} \cdots h'_{n} l'_{1} \cdots l_{m}}(\Lambda x + a)
\end{equation}
Il modo in cui trasformano gli operatori di creazione e distruzione sotto trasformazione di Lorentz è già fissato, e quindi impone un vincolo sulla forma dei \( u_{l} \) e \( v_{l} \) in modo che tale regola sia compatibile con la regola di trasformazione dei campi. Reintroducendo il creatore associato alla normalizzazione relativistica \( \alpha^{\dagger} = \sqrt{2E} a^{\dagger} \) e usando le proprietà di trasformazione di \( \alpha \) sotto Poincaré
\begin{align*}
  U_{0}(\Lambda, a) \psi_{l}^{+}(x) U_{0}^{-1}(\Lambda, a) &=  \sum_{\sigma, n} \int \Dp \, v_{l}(x, \vec{p}, \sigma, n) U_{0}(\Lambda, a) a^{\dagger}(\vec{p}, \sigma, n) U_{0}^{-1}(\Lambda, a) \\
                                                           &=  \sum_{\sigma, n} \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}2 E_{\vec{p}}} \, v_{l}(x, \vec{p}, \sigma, n) U_{0}(\Lambda, a) \alpha^{\dagger}(\vec{p}, \sigma, n) U_{0}^{-1}(\Lambda, a) \\
                                                           &= \sum_{\sigma, n} \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2} 2 E_{\vec{p}}} \, v_{l}(x, \vec{p}, \sigma, n) e^{-ia_{\mu} (\Lambda p)^{\mu}}\sum_{\sigma'} \mathcal{D}_{\sigma' \sigma}^{(j)}(W(\Lambda, p)) \alpha^{\dagger}(\vec{(\Lambda p)}, \sigma', n)
\end{align*}
La misura invariante di Lorentz pertanto
\begin{equation}
    \frac{\dif[3]{\vec{p}}}{2 E_{\vec{p}}} = \frac{\dif[3]{\vec{q}}}{2 E_{\vec{q}}} \qquad q = \Lambda p
\end{equation}
Quindi
\begin{align*}
  \psi^{+}_{l}(\Lambda x + a) &= \sum_{\sigma, n} \int \frac{\dif[3]{\vec{q}}}{(2\pi)^{3/2}\sqrt{2E_{\vec{q}}}} \, v_{l}(\Lambda x + a, \vec{q}, \sigma, n) a^{\dagger}(\vec{q}, \sigma, n) \\
                              &= \sum_{\sigma, n} \int \frac{\dif[3]{\vec{q}}}{(2\pi)^{3/2}2E_{\vec{q}}} \, v_{l}(\Lambda x + a, \vec{q}, \sigma, n) \alpha^{\dagger}(\vec{q}, \sigma, n) \\
                              &= \sum_{\sigma, n} \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}2 E_{\vec{p}}} \, v_{l}(\Lambda x + a, \vec{(\Lambda p)}, \sigma, n) \alpha^{\dagger}(\vec{(\Lambda p)}, \sigma, n)
\end{align*}
Per consistenza deve quindi essere (il conto per \( u_{l} \) è  analogo)
\begin{align}
  \sum_{l'}  D_{l l'}^{-}(\Lambda^{-1}) v_{l'}(\Lambda x + a, \vec{(\Lambda p)}, \sigma', n)
  & =   e^{-ia_{\mu} (\Lambda p)^{\mu}}  \sum_{\sigma} \mathcal{D}_{\sigma' \sigma}^{(j)}(W(\Lambda, p)) v_{l}(x, \vec{p}, \sigma, n) \\
  \sum_{l'}  D_{l l'}^{+}(\Lambda^{-1}) u_{l'}(\Lambda x + a, \vec{(\Lambda p)}, \sigma', n)
  &=   e^{ia_{\mu} (\Lambda p)^{\mu}} \sum_{\sigma} \left[\mathcal{D}^{(j)}(W(\Lambda, p))\right]^{\dagger}_{\sigma' \sigma} u_{l}(x, \vec{p}, \sigma, n)
\end{align}
Possiamo ancora fare delle deduzioni generali a partire da queste regole di trasformazione.
\begin{itemize}
    \item \emph{Traslazioni}: se consideriamo traslazioni pure \( \Lambda = 1 \) si ha \( W(\Lambda, p) = 1 \) e di conseguenza anche la rappresentazione \( D^{\pm} \) e le \( \mathcal{D}^{(j)} \) sono l'identità. Pertanto otteniamo
    \begin{equation}
        v_{l}(x + a, \vec{p}, \sigma, n) = e^{-ia_{\mu} p^{\mu}} v_{l}(x, \vec{p}, \sigma, n) \qquad  \Rightarrow \qquad v_{l}(x, \vec{p}, \sigma, n) = \tilde{v}_{l}(\vec{p}, \sigma, n) e^{-i p x}
    \end{equation}
    e analogamente per \( u_{l} \). Di conseguenza, a meno di ridefinire i coefficienti poniamo
    \begin{align*}
      \hat{\psi}^{+}_{l}(x) &= \sum_{\sigma, n} \int \Dp u_{l}(\vec{p}, \sigma, n) a(\vec{p}, \sigma, n) e^{ipx}
      \\
      \hat{\psi}^{-}_{l}(x) &= \sum_{\sigma, n} \int \Dp v_{l}(\vec{p}, \sigma, n) a^{\dagger}(\vec{p}, \sigma, n) e^{-ipx}
    \end{align*}
    ovvero i campi di creazione e annichilazione non sono altro che la trasformata di Fourier degli operatori di creazione e distruzione. In queste scritture si sottintende che l'impulso a esponente è \emph{on-shell}, ovvero che \( p^{2} = m^{2} \). \\

    La definizione non è chiaramente univoca perché possiamo riassorbire fattori convenzionali. Io sto usando una convenzione in cui se metto i creatori relativistici l'integrale è Lorentz invariante più la convenzione, ci metto anche il \( (2\pi)^{3/2} \) perché fa comodo. Questa è la convenzione dei Coleman. Weinberg non mette il \( (2\pi)^{3/2}\sqrt{2E} \) e quindi ha sempre dei fattori di radice dell'energia in giro che fanno un po' schifo. Vedere le note di Niccolò per le varie convenzioni. \\

    Il fattore di trasformazione \( e^{\pm i a_{\mu}(\Lambda p)^{\mu}} \) è allora banale e ci basta considerare le proprietà di trasformazione sotto le sole trasformazioni di Lorentz
    \begin{align}
      \label{eq:vl-trasformazione-lorentz}
      \sum_{l'}  D_{l l'}^{-}(\Lambda^{-1}) v_{l'}(\vec{(\Lambda p)}, \sigma', n)
      &=  \sum_{\sigma} \mathcal{D}_{\sigma' \sigma}^{(j)}(W(\Lambda, p)) v_{l}(\vec{p}, \sigma, n) \\
      \label{eq:ul-trasformazione-lorentz}
      \sum_{l'}  D_{l l'}^{+}(\Lambda^{-1}) u_{l'}(\vec{(\Lambda p)}, \sigma', n)
      &= \sum_{\sigma} \left[\mathcal{D}^{(j)}(W(\Lambda, p))\right]^{\dagger}_{\sigma' \sigma} u_{l}(\vec{p}, \sigma, n)
    \end{align}
    \item \emph{Boost}: consideriamo il caso in cui \( \Lambda = L(p) \) è il boost da \( p \) al vettore standard \( k \). Ricordiamo che \( W(L(p), k) = L^{-1}(L(p) k) L(p) L(k) = 1 \) quindi
    \begin{equation}
        \sum_{l'}  D_{l l'}^{-}(L(p)^{-1}) v_{l'}(\vec{p}, \sigma', n)
        =  v_{l}(\vec{k}, \sigma', n)
    \end{equation}
    Usando che \( D_{l l'}^{-}(\Lambda) D_{l' l''}^{-}(\Lambda^{-1}) = \delta_{l l''} \) l'espressione precedente è equivalente a
    \begin{equation}
        v_{l''}(\vec{p}, \sigma', n) =  \sum_{l''}  D_{l'' l}^{-}(L(p)) v_{l}(\vec{k}, \sigma', n)
    \end{equation}
    ovvero, facendo anche il conto analogo per \( u_{l} \) si ha
    \begin{align}
      v_{l}(\vec{p}, \sigma, n) &= \sum_{l}  D_{l l'}^{-}(L(p)) v_{l'}(\vec{k}, \sigma, n) \\
      u_{l}(\vec{p}, \sigma, n) &= \sum_{l}  D_{l l'}^{+}(L(p)) u_{l'}(\vec{k}, \sigma, n)
    \end{align}
    Nel caso massivo \( \vec{k} = \vec{0} \), mentre nel caso massless \( \vec{k} = (0, 0, \kappa) \).

    \item \emph{Rotazioni}: nel caso massivo mettiamoci nel frame in cui \( \vec{p} = 0 \) e prendiamo  \( \Lambda \) una pura rotazione spaziale. Allora \( W(\Lambda, p) = R \in \SO(3) \) e le matrici \( \mathcal{D}^{s}(R) \) sono le \emph{Wigner-D}. Otteniamo allora
    \begin{align}
      \sum_{l'}  D_{l l'}^{-}(R^{-1}) v_{l'}(\vec{0}, \sigma', n)
      &= \sum_{\sigma} \mathcal{D}_{\sigma' \sigma}^{(j)}(R) v_{l}(\vec{0}, \sigma, n) \\
      \sum_{l'}  D_{l l'}^{+}(R^{-1}) u_{l'}(\vec{0}, \sigma', n)
      &=   \sum_{\sigma} \left[\mathcal{D}^{(j)}(R)\right]^{\dagger}_{\sigma' \sigma} u_{l}(\vec{0}, \sigma, n)
    \end{align}
\end{itemize}

Per ora abbiamo solo imposto la covarianza sotto trasformazioni di Lorentz. Tuttavia è anche necessario che la densità di interazione soddisfi anche la proprietà di causalità, cosa che il generale impone degli ulteriori vincoli sulle funzioni \( u_{l} \) e \( v_{l} \). In particolare per particelle bosoniche/fermioniche deve essere
\begin{equation}
    [a(\vec{p}, \sigma, n), a^{\dagger}(\vec{p}', \sigma', n')]_{\mp} = \delta_{\sigma \sigma'}\delta_{n n'} \delta^{(3)}(\vec{p} - \vec{p}')
\end{equation}
da cui
\begin{equation}
    [\psi_{l}^{+}(x), \psi_{l'}^{-}(y)]_{\mp} = \frac{1}{(2\pi)^{3}}\sum_{\sigma, n} \int \frac{\dif[3]{\vec{p}}}{2 E_{\vec{p}}} u_{l}(\vec{p}, \sigma, n) v_{l'}(\vec{p}, \sigma, n) e^{ip(x - y)}
\end{equation}
dove \( [\cdot, \cdot]_{-} \) è il commutatore \( [\cdot, \cdot] \), mentre \( [\cdot, \cdot]_{+} \) è l'anticommutatore \(  \{\cdot, \cdot\}  \). In generale tale commutatore/anticommutatore è non nullo per distanze space-like tra \( x \) e \( y \). Una possibilità è supporre che i campi entrino nell'hamiltoniana nella combinazione lineare
\begin{equation}
    \psi_{l}(x) = \kappa_{l} \psi_{l}^{+}(x) + \lambda_{l}\psi_{l}^{-}(x)
\end{equation}
che soddisfi in modo tale che
\begin{equation}
    [\psi_{l}(x), \psi_{l'}(y)]_{\mp} = [\psi_{l}(x), \psi_{l'}^{\dagger}(y)]_{\mp} = 0 \qquad \text{se } (x - y)^{2} < 0
\end{equation}
Vedremo nei casi specifici quali sono le combinazioni lineari opportune che realizzano tale richiesta di causalità. \\

Osserviamo incidentalmente che i campi così costruiti soddisfano automaticamente l'equazione di Klein-Gordon. Infatti
\begin{equation}
    (\Box + m^{2}) \psi_{l}^{-}(x) = \sum_{\sigma, n} \int \Dp v_{l}(\vec{p}, \sigma, n) a^{\dagger}(\vec{p}, \sigma, n) (-p^{2} + m^{2}) e^{-i p x} = 0
\end{equation}
essendo l'impulso on-shell. Similmente per \( \psi_{l}^{+} \) e quindi anche per \( \psi_{l} \) essendo combinazione lineare. \\

Motivazione estremamente oscura circa la necessità delle antiparticelle. Supponiamo che le particelle a cui siamo interessati abbiano una carica
\begin{equation}
    [Q, a^{\dagger}(\vec{p}, \sigma, n)] = q(n) a^{\dagger}(\vec{p}, \sigma, n) \qquad
    [Q, a(\vec{p}, \sigma, n)] = - q(n) a(\vec{p}, \sigma, n)
\end{equation}
Per essere un buon numero quantico la carica deve rimanere conservata, quindi deve essere
\begin{equation}
    [Q, \mathscr{V}(x)] = 0
\end{equation}
dove \( \mathscr{V}(x) \) è il polinomio di \( \psi_{l} \) e il suo hermitiano coniugato, simile a quanto scritto nella~\eqref{eq:interazione}
\begin{equation}
    \mathscr{V}(x) = \sum_{n, m} \sum_{h_{j}}\sum_{l_{i}}  g_{h_{1} \cdots h_{n} l_{1} \cdots l_{m}}(x) \psi_{h_{1}}(x) \cdots \psi_{h_{n}}(x) \psi_{l_{1}}^{\dagger}(x) \cdots \psi_{l_{m}}^{\dagger}(x)
\end{equation}
La cosa più semplice è supporre che i campi \( \psi_{l} \) siano anch'essi autostati della carica
\begin{equation}
    [Q, \psi_{l}^{\dagger}(x)] = q_{l} \psi_{l}^{\dagger}(x)
\end{equation}
e quindi, affinché la carica sia conservata è sufficiente richiedere che gli unici coefficienti non nulli siano quelli tali che
\begin{equation}
    q_{l_{1}} + \cdots + q_{l_{m}} - q_{h_{1}} - \cdots - q_{h_{m}} = 0
\end{equation}
Scrivendo \( \psi_{l} \) in termini di \( \psi_{l}^{\pm} \) la condizione di essere un campo carico è
\begin{align}
  &q_{l}\left(\kappa_{l} \sum_{\sigma, n}u_{l}(\vec{p}, \sigma, n) a(\vec{p}, \sigma, n)
    + \lambda_{l} \sum_{\sigma', n'} v_{l}(\vec{p}, \sigma', n') a^{\dagger}(\vec{p}, \sigma', n') \right) \\ \nonumber
  &\qquad = - \kappa_{l} \sum_{\sigma, n} q(n)  u_{l}(\vec{p}, \sigma, n) a(\vec{p}, \sigma, n)
    + \lambda_{l} \sum_{\sigma', n'} q(n')v_{l}(\vec{p}, \sigma', n') a^{\dagger}(\vec{p}, \sigma', n')
\end{align}
dove abbiamo esplicitato il fatto che in linea di principio i campi \( \psi^{\pm}_{l} \) hanno un diverso contenuto di particelle. In particolare affinché sia soddisfatta tale condizione una possibilità è che per \( \psi^{-} \) la somma sia ristretta alle particelle con \( q(n') = q_{l} \) mentre quella per \( \psi^{+} \) alle particelle con \( q(n) = - q_{l} \).

Questo dovrebbe giustificare il fatto che se vogliamo conservare la carica allora il campo associato alle particelle in questione deve contenere sia la particella che l'antiparticella (particella con gli stessi numeri quantici tranne la carica che è di segno opposto).

Nel seguito quello che facciamo e studiare le rappresentazioni irriducibili finito-dimensionali del gruppo di Lorentz e trovare quindi i campi, cioè i coefficienti \( u_{l} \) e \( v_{l} \) compatibili con le~\eqref{eq:vl-trasformazione-lorentz},~\eqref{eq:vl-trasformazione-lorentz} e i coefficienti \( \kappa_{l} \) e \( \lambda_{l} \).

\subsection{\textcolor{red}{(TODO)} Rappresentazioni finito dimensionali del gruppo di Lorentz}
Le solite cose su \( \mathfrak{so}(3, 1)_{\R} \simeq \mathfrak{su}(2)_{\C} \oplus \mathfrak{su}(2)_{\C} \) e classificazione delle rappresentazioni come \( (s_{+}, s_{-}) \)

\subsection{Campo scalare}
\subsubsection{Campo scalare reale}
Prendiamo la rappresentazione banale \( D^{\pm}(\Lambda) = 1 \). Ristretto alle rotazioni questo implica che è \( j = 0 \) e \( \sigma = 0 \), ovvero le particelle descritte da tale campo hanno spin nullo. Parliamo quindi di particelle e campi \emph{scalari}. Consideriamo inoltre il caso in cui sia presente 1 sola specie (no antiparticelle) e ``lasciamo cadere il sottoscritto \( n \)'' (cit). Scriviamo quindi semplicemente
\begin{equation}
    \psi_{l} \to \phi \qquad u_{l}(\vec{p}, \sigma, n) \to u(\vec{p}) \qquad v_{l}(\vec{p}, \sigma, n) \to v(\vec{p}) \qquad a(\vec{p}, \sigma, n) \to a(\vec{p})
\end{equation}
Dalla regola di trasformazione sotto boost abbiamo
\begin{equation}
    u(\vec{p}) = u(\vec{0}) \qquad v(\vec{p}) = v(\vec{0})
\end{equation}
e per convezione prendiamo \( u(\vec{0}) = v(\vec{0}) = 1 \) così i campi di creazione e distruzione sono
\begin{equation}
    \hat{\phi}^{+}(x) = \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}\sqrt{2E_{\vec{p}}}} a(\vec{p}) e^{ip\cdot x} \qquad \hat{\phi}^{-}(x) = \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}\sqrt{2E_{\vec{p}}}} a^{\dagger}(\vec{p}) e^{-ip\cdot x} = \left[\hat{\phi}^{+}(x)\right]^{\dagger}
\end{equation}
ove, al solito, \( p^{2} = m^{2} \). L'hamiltoniana costruita come polinomio di \( \phi^{\pm} \) sarà automaticamente scalare di Lorentz, ma non necessariamente causale. Infatti se fosse un polinomio solamente di \( \phi^{+} \) o di \( \phi^{-} \), non ci sarebbero problemi in quanto \( [\phi^{\pm}(x), \phi^{\pm}(y)] = 0 \) oppure \( \{\phi^{\pm}(x), \phi^{\pm}(y)\} = 0 \). Tuttavia se vogliamo che \( \mathscr{V} \) sia hermitiana allora deve coinvolgere necessariamente sia \( \phi^{+} \) che \( \phi^{-} \) essendo questi singolarmente non hermitiani. Osserviamo però che è
\begin{equation}
    [\phi^{+}(x), \phi^{-}(y)] = \Delta_{+}(x - y) = \frac{1}{(2\pi)^{3}}\int \frac{\dif[3]{\vec{p}}}{2 E_{\vec{p}}} e^{ip \cdot (x - y)}
\end{equation}
o espressione analoga per operatori fermionici, che è non nulla per \( (x - y)^{2} < 0 \). Se invece consideriamo una combinazione lineare
\begin{equation}
    \phi(x) = \kappa \phi^{+}(x) + \lambda \phi^{-}(x)
\end{equation}
allora è
\begin{equation}
    [\phi(x), \phi(y)] = 0 \qquad \text{oppure} \qquad \{\phi(x), \phi(y)\} = 2\kappa \lambda \Delta_{+}(x - y)
\end{equation}
Quindi il campo può essere solo \( bosonico \). Inoltre affinché \( \phi \) sia hermitiano deve esser \( \lambda = \kappa^{*} \). Possiamo sempre ridefinire il campo a meno di una costante globale e gli operatori \( a \) e \( a^{\dagger} \) a meno di una fase uguale e opposta. Di conseguenza il \emph{campo scalare reale} causale è
\begin{equation}
    \phi(x) = \phi^{+}(x) + \phi^{-}(x) = \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}\sqrt{2 E_{\vec{p}}}} \left(a(\vec{p}) e^{i p \cdot x} + a^{\dagger}(\vec{p}) e^{-i p \cdot x}\right)
\end{equation}

\subsubsection{Campo scalare complesso}

Questo campo descrive necessariamente particelle senza carica. Supponiamo infatti che esista un operatore di carica \( Q \) e che gli stati di singola particella siano autostati della carica
\begin{equation}
    Q \ket{\vec{p}, q} = q \ket{\vec{p}, q} \qquad \Rightarrow \qquad
    \begin{cases}
        [Q, a^{\dagger}(\vec{p}, q)] = q a^{\dagger}(\vec{p}, q) \\
        [Q, a(\vec{p}, q)] = -q a(\vec{p}, q)
    \end{cases}
\end{equation}
Di conseguenza è anche
\begin{equation}
    [Q, \phi^{\pm}(x)] = \mp q \phi^{\pm}(x)
\end{equation}
dove in questo caso nella decomposizione dei campi \( \phi^{\pm} \) in termini di operatori di creazione e distruzione non sommiamo sul numero quantico di carica. Di fatto stiamo considerando un capo a ``carica fissata''.  Se consideriamo solo il campo \( \phi = \phi^{+} + \phi^{-} \) allora è automaticamente \( [Q, \phi(x)] = 0 \).

Per avere un campo ``carico'' dobbiamo considerare un sistema composto da due bosoni massivi di spin zero e carica opposta. Se \( a, a^{\dagger} \) e \( b, b^{\dagger} \) sono i rispettivi operatori di creazione e distruzione, questi devono soddisfare
\begin{equation}
    [Q, a^{\dagger}(\vec{p}, q)] = q a^{\dagger}(\vec{p}, q) \qquad [Q, b^{\dagger}(\vec{p}, q)] = -q b^{\dagger}(\vec{p}, q)
\end{equation}
da cui se \( \phi^{\pm}_{a} \) e \( \phi^{\pm}_{b} \) sono i campi di creazione e distruzione costruiti rispettivamente con \( a, a^{\dagger} \) e con \( b, b^{\dagger} \) allora si ha
\begin{equation}
    [Q, \phi_{a}^{\pm}(x)] = \pm q \phi_{a}^{\pm}(x) \qquad [Q, \phi_{b}^{\pm}(x)] = \mp q \phi_{b}^{\pm}(x)
\end{equation}
Di conseguenza se consideriamo i campi
\begin{equation}
    \begin{cases*}
        \phi(x) = \phi_{a}^{+}(x) + \phi_{b}^{-}(x) \\
        \phi^{*}(x) = [\phi(x)]^{\dagger} = \phi_{b}^{+}(x) + \phi_{a}^{-}(x)
    \end{cases*}
\end{equation}
sono campi di carica uguale e opposta \( [Q, \phi(x)] = q \phi(x) \) e sono ancora campi causali essendo
\begin{gather*}
    [\phi(x), \phi(y)] = 0 \qquad [\phi^{*}(x), \phi^{*}(y)] = 0 \\
    [\phi(x), \phi^{*}(y)] = [\phi_{a}^{+}(x), \phi_{a}^{-}(y)] + [\phi_{b}^{-}(x), \phi_{b}^{+}(y)] = \Delta_{+}(x - y) - \Delta_{+}(x - y) = 0
\end{gather*}
Dove abbiamo assunto che le particelle bosoniche in questione avessero la stessa massa. Anche in questo caso si più considerare la possibilità che le particelle siano fermioniche, ma anche qui tale condizione non è compatibile con quella di causalità. Tuttavia in questo caso il campo non è più hermitiano, quindi bisognerà costruire la densità hamiltoniana con le opportune combinazioni di \( \phi \) e \( \phi^{*} \) che la rendono autoaggiunta (per esempio andrebbe un termine della forma \( p(\phi^{*}(x) \phi(x)) \) dove \( p \) è un polinomio).

Più esplicitamente abbiamo ottenuto la forma del \emph{campo scalare complesso} causale
\begin{equation}
    \phi(x) = \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}\sqrt{2 E_{\vec{p}}}} \left(a(\vec{p}) e^{i p \cdot x} + b^{\dagger}(\vec{p}) e^{-i p \cdot x}\right)
\end{equation}

\subsubsection{Parità e coniugazione di carica}

\subsection{Campo vettoriale}
Prendiamo la rappresentazione fondamentale
\begin{equation}
    {{[D^{\pm}(\Lambda)]}^{\mu}}_{\nu} = {\Lambda^{\mu}}_{\nu}
\end{equation}
In linea di principio abbiamo 4 possibili valori per \( l \) che indicizziamo con un indice greco quadri-vettoriale. Anche qui supponiamo per ora di lavorare con una singola specie e droppiamo il label \( n \). I campi di creazione e distruzione si scrivono quindi come
\begin{equation}
    \varphi^{+}_{\mu}(x) = \sum_{\sigma} \int \Dp u_{\mu}(\vec{p}, \sigma) a(\vec{p}, \sigma) e^{i p \cdot x} \qquad \varphi^{-}_{\mu}(x) = \sum_{\sigma} \int \Dp v_{\mu}(\vec{p}, \sigma) a^{\dagger}(\vec{p}, \sigma) e^{-i p \cdot x}
\end{equation}
Sotto boost \( L(p) \) otteniamo
\begin{equation}
    u^{\nu}(\vec{p}, \sigma) = {{L(p)}^{\mu}}_{\nu} \, u^{\nu}(\vec{0}, \sigma) \qquad v^{\nu}(\vec{p}, \sigma) = {{L(p)}^{\mu}}_{\nu} \, v^{\nu}(\vec{0}, \sigma)
\end{equation}
Consideriamo le proprietà di trasformazione sotto rotazioni, per \( u^{\mu} \) abbiamo
\begin{equation}
    \label{eq:campo-vettoriale-rotazione}
    {{\mathcal{R}}^{\mu}}_{\nu} u^{\nu}(\vec{0}, \sigma)
    = \sum_{\sigma'} \mathcal{D}_{\sigma \sigma'}^{(j)}(R) u^{\mu}(\vec{0}, \sigma')
    \qquad
    \text{ove}
    \quad
    \mathcal{R} =
    \left(
        \begin{array}{c|ccc}
          1 & & &\\  \hline
            & & & \\
            & & R & \\
            & & &
        \end{array}
    \right)
\end{equation}
Per una rotazione infinitesima \( R_{ij} = \delta_{ij} + i \theta_{k} {(M_{k})}_{ij} = \delta_{ij} + \epsilon_{ijk} \theta_{k} \) essendo \( {(M_{k})}_{ij} = -i \epsilon_{ijk} \) i generatori della rappresentazione fondamentale di \( \SO(3) \). Se \( \{J^{(j)}_{k}\} \) sono i generatori della rappresentazione di dimensione \( 2j + 1 \) di \( \SO(3) \) allora è \( \mathcal{D}^{(j)}_{\sigma \sigma'}(R) \simeq \delta_{\sigma \sigma'} + i \theta_{k} {[J^{(j)}_{k}]}_{\sigma \sigma'} \). Pertanto la~\eqref{eq:campo-vettoriale-rotazione} diventa
\begin{equation}
    \label{eq:campo-vettoriale-rotazione-infinitesima}
    \begin{dcases*}
        0 = \sum_{\sigma'} {\left[J^{(j)}_{k}\right]}_{\sigma \sigma'} u^{0}(\vec{0}, \sigma') \\
        \epsilon_{ijk} u^{j}(\vec{0}, \sigma)
        = i \sum_{\sigma'} {\left[J^{(j)}_{k}\right]}_{\sigma \sigma'} u^{i}(\vec{0}, \sigma')
    \end{dcases*}
\end{equation}
Da cui ricaviamo
\begin{equation}
    \begin{dcases*}
        \sum_{\sigma'}  {\left[J^{2}_{(j)}\right]}_{\sigma \sigma'} u^{0}(\vec{0}, \sigma') = 0 \\
        \sum_{\sigma'}  {\left[J^{2}_{(j)}\right]}_{\sigma \sigma'} u^{i}(\vec{0}, \sigma') = 2 u^{i}(\vec{0}, \sigma)
    \end{dcases*}
\end{equation}
dove \( J^{2}_{(j)} \) è la rappresentazione di dimensione \( 2j + 1 \) dell'operatore di Casimir quadratico di \( \SO(3) \). Ricordando che è \( {[J^{2}_{(j)}]}_{\sigma \sigma'} = j(j+1) \delta_{\sigma \sigma'} \) otteniamo allora
\begin{equation}
    \begin{dcases*}
        j(j+1) u^{0}(\vec{0}, \sigma) = 0 \\
        [j(j+1) - 2] u^{i}(\vec{0}, \sigma) = 0
    \end{dcases*}
\end{equation}
Si ottengono equazioni identiche anche per \( v^{\mu} \).

Per avere soluzioni non nulle ci sono solo due possibilità.
\begin{enumerate}
    \item \emph{Spin zero}: \( j = 0 \) (da cui \( \sigma = 0 \)) e \( u^{i}(\vec{0}) = v^{i}(\vec{0}) = 0 \). Convenzionalmente poniamo
    \begin{equation}
        u^{0}(\vec{0}) = \frac{i}{(2\pi)^{3/2}} \qquad v^{0}(\vec{0}) = -\frac{i}{(2\pi)^{3/2}}
    \end{equation}
    da cui per trasformazioni sotto boost si ottiene
    \begin{equation}
        u^{\mu}(\vec{p}) = \frac{i}{(2\pi)^{3/2}}  p^{\mu} \qquad v^{\mu}(\vec{p}) = -\frac{i}{(2\pi)^{3/2}}  p^{\mu}
    \end{equation}
    Ma allora i campi di creazione e distruzione non sono altro che la derivata dei campi scalari
    \begin{equation}
        \varphi^{+}_{\mu}(x) = \int \frac{\dif[3]{\vec{p}}}{(2\pi)^{3/2}\sqrt{2 E_{\vec{p}}}} ip_{\mu} a(\vec{p}) e^{i p \cdot x} = \partial_{\mu} \phi^{+}(x) \qquad \varphi^{-}_{\mu}(x) = \partial_{\mu} \phi^{-}(x)
    \end{equation}
    Di conseguenza il campo vettoriale causale di spin zero non è altro che il gradiente del campo scalare causale
    \begin{equation}
        \varphi_{\mu}(x) = \varphi^{+}_{\mu}(x) + \varphi^{-}_{\mu}(x) = \partial_{\mu} \phi(x)
    \end{equation}

    \item \emph{Spin 1}: \( j = 1 \) da cui \( \sigma = 0, \pm 1 \) e \( u^{0}(\vec{0}, \sigma) = v^{0}(\vec{0}, \sigma) = 0 \)
    Possiamo scegliere convenzionalmente
    \begin{equation}
        u^{\mu}(\vec{0}, 0) = v^{\mu}(\vec{0}, 0) =
        \begin{pmatrix}
            0 \\
            0 \\
            0 \\
            1
        \end{pmatrix}
    \end{equation}
    Usando allora la~\eqref{eq:campo-vettoriale-rotazione-infinitesima} e la rappresentazione fondamentale degli operatori di salita e di discesa \( J^{(1)}_{\pm} = J_{1}^{(1)} \pm i J^{(2)}_{2} \) si ottiene
    \begin{equation}
        u^{\mu}(\vec{0}, \pm) = - v^{\mu}(\vec{0}, \mp) = \mp\frac{1}{\sqrt{2}}
        \begin{pmatrix}
            0 \\
            1 \\
            \pm i \\
            0
        \end{pmatrix}
    \end{equation}
    Usando le proprietà di trasformazione sotto boost si ottiene infine la forma dei campi di creazione e distruzione
    \begin{equation}
        \varphi^{+}_{\mu}(x) = \sum_{\sigma=0, \pm 1} \int \Dp e_{\mu}(\vec{p}, \sigma) a(\vec{p}, \sigma) e^{i p \cdot x} \qquad \varphi^{-}_{mu}(x) = \left[\varphi^{+}_{mu}(x)\right]^{\dagger}
    \end{equation}
    dove \( e^{\mu}(\vec{p}, \sigma) = {L(p)^{\mu}}_{\nu} e^{\nu}(\vec{0}, \sigma)\) e
    \begin{equation}
        e^{\mu}(\vec{0}, 0) =
        \begin{pmatrix}
            0 \\
            0 \\
            0 \\
            1
        \end{pmatrix}
        \qquad
        e^{\mu}(\vec{0}, \pm1) =
        \mp \frac{1}{\sqrt{2}}
        \begin{pmatrix}
            0 \\
            1 \\
            \pm i \\
            0
        \end{pmatrix}
    \end{equation}
    Al solito \( \varphi_{\mu}^{\pm} \) non commutato tra di loro ma è
    \begin{equation}
        [\varphi^{+}_{\mu}(x), \varphi^{-}_{\nu}(y)] = \frac{1}{(2\pi)^{3}} \int \frac{\dif[3]{\vec{p}}}{2 E_{\vec{p}}}  \Pi_{\mu \nu}(\vec{p}) e^{i p \cdot (x - y)}
    \end{equation}
    ove
    \begin{equation}
        \Pi_{\mu \nu}(\vec{p}) = \sum_{\sigma=0, \pm1} e_{\mu}(\vec{p}, \sigma) e_{\nu}^{*}(\vec{p}, \sigma)
    \end{equation}
    Tale tensore è il proiettore nella direzione trasversa a \( p \): infatti nel rest-frame si mostra esplicitamente che è
    \begin{equation}
        \Pi_{\mu \nu}(\vec{k}) =
        \begin{pmatrix}
            0 & & &  \\
            & 1 & & \\
            & & 1 & \\
            & & & 1
        \end{pmatrix}
        = \eta_{\mu\nu} - \frac{k_{\mu} k_{\nu}}{m^{2}}
        \qquad \Rightarrow \qquad
        \Pi_{\mu \nu}(\vec{p}) = \eta_{\mu\nu} - \frac{p_{\mu} p_{\nu}}{m^{2}}
    \end{equation}
    Pertanto
    \begin{equation}
        [\varphi^{+}_{\mu}(x), \varphi^{-}_{\nu}(y)] = \left[\eta_{\mu\nu} + \frac{\partial_{\mu} \partial_{\nu}}{m^{2}}\right] \Delta_{+}(x - y)
    \end{equation}
    Lo stesso risultato si ottiene per particelle fermioniche sostituendo il commutatore con l'anticommutatore. Quindi, come per il campo scalare, è necessario considerare una combinazione lineare di campi di creazione e di distruzione. Se c'è un solo campo si trova al solito che l'unica possibilità è il \emph{campo vettoriale reale causale bosonico} della forma
    \begin{equation}
        B_{\mu}(x) = \varphi^{+}_{\mu}(x) + \varphi^{-}_{\mu}(x) = \sum_{\sigma=0, \pm 1} \int \Dp \left(e_{\mu}(\vec{p}, \sigma) a(\vec{p}, \sigma) e^{i p \cdot x} + e_{\mu}^{*}(\vec{p}, \sigma) a^{\dagger}(\vec{p}, \sigma) e^{-i p \cdot x} \right)
    \end{equation}
    Anche questo è un campo senza carica, se voglio un campo che porti carica devo considerare 2 particelle bosoniche di spin 1 e ripete quanto fatto per il campo scalare complesso, ottenendo
    \begin{equation}
        B_{\mu}(x) = \varphi^{+}_{\mu}(x) + \varphi^{-}_{\mu}(x) = \sum_{\sigma=0, \pm 1} \int \Dp \left(e_{\mu}(\vec{p}, \sigma) a(\vec{p}, \sigma) e^{i p \cdot x} + e_{\mu}^{*}(\vec{p}, \sigma) b^{\dagger}(\vec{p}, \sigma) e^{-i p \cdot x} \right)
    \end{equation}


    Osserviamo infine che, nonostante \( B_{\mu} \) abbia 4 componenti, in realtà porta solo 3 gradi di libertà fisici e infatti nel \emph{rest frame} \( B_{0} = 0 \).

    Possiamo vedere questo come un vincolo delle equazioni di campo. Oltre all'equazione di Klein-Gordon, il campo scalare vettoriale massivo soddisfa
    \begin{equation}
        \partial_{\mu} B^{\mu}(x) = 0
    \end{equation}
    come conseguenza del fatto che
    \begin{equation}
        p_{\mu} e^{\mu}(\vec{p}, \sigma) = p_{\mu} {{L(p)}^{\mu}}_{\nu} e^{\nu}(\vec{0}, \sigma) = k_{\nu} e^{\nu}(\vec{0}, \sigma) = 0
    \end{equation}
    essendo \( k = (m, \vec{0}) \).
\end{enumerate}

Osserviamo che il limite \( m \to 0 \) è non banale e quindi non possiamo semplicemente ottenere il campo vettoriale massless come limite. Infatti se consideriamo un'hamiltoniana che contenga un'interazione campo-corrente \( \mathscr{V} = J^{\mu}(x) B_{\mu}(x) \), al tree-level le sezioni d'urto sono proporzionali a
\begin{equation}
    \sum_{\sigma} \abs{\braket{\tilde{J}^{\mu}(p)} e_{\mu}^{*}(\vec{p}, \sigma)}^{2} = \braket{\tilde{J}^{\mu}(p)}  \braket{\tilde{J}^{\nu}(p)}^{*} \Pi_{\mu\nu}(\vec{p})
\end{equation}
che diverge nel limite \( m \to 0 \). Possiamo evitare questo fatto possiamo supporre che l'accoppiamento sia con una corrente classicamente conservata
\begin{equation}
    \partial_{\mu} J^{\mu}(x) = 0 \qquad \Rightarrow \qquad p_{\mu} \tilde{J}^{\mu}(p) = 0
\end{equation}
Weinberg afferma che la conservazione della corrente è equivalente a eliminare il modo di elicità nulla dal campo vettoriale massivo. \\

Cose sulle simmetrie discrete.

\subsection{\textcolor{red}{(TODO)} Campo spinoriale}
Rappresentazione \( (0, 1/2) \) e \( (1/2, 0) \). Algebra di Clifford e matrici \( \gamma^{\mu} \). Rappresentazione di Dirac.

Un botto di cose con le simmetrie discrete.

\subsection{Campi massless}
Per spin \( j \leq 1/2 \) si riesce a fare il limite di massa nulla, ma per spin \( j \geq 1 \) non è più possibile e le cose si fanno più complicate.

In particolare se prendiamo la rappresentazione fondamentale \( D(\Lambda) = \Lambda \). Prova a imporre le proprietà di trasformazione giuste nel frame speciale per due particolari trasformazioni
\begin{equation}
    {\mathcal{R}(\theta)^{\mu}}_{\nu} e^{\nu}(\vec{k}, h) = e^{i h \theta} e^{\mu}(\vec{k}, h) \qquad
    {S(\alpha, \beta)^{\mu}}_{\nu} e^{\nu}(\vec{k}, h) =  e^{\mu}(\vec{k}, h)
\end{equation}
che sono una rotazione \( \mathcal{R} \) attorno a \( x_{3} \) e un boost \( S \) nel piano \( x_{1}x_{2} \). Afferma che la condizione sulle rotazioni gli impone \( h = \pm 1 \) e
\begin{equation}
    e^{\mu}(\vec{k}, \pm 1) = \frac{1}{\sqrt{2}}
    \begin{pmatrix}
        0 \\
        1 \\
        \pm i \\
        0
    \end{pmatrix}
\end{equation}
ma che questa relazione non è compatibile con la regola di trasformazione sotto boost.

Afferma ``let's temprarely close our eyes'', prende i vettori di base di sopra e guarda cosa succede sotto boost:
\begin{equation}
    {\Lambda^{\mu}}_{\nu} e^{\nu}(\vec{p}, \pm 1) = {L(\Lambda p)^{\mu}}_{\nu} {W(\Lambda, p)^{\nu}}_{\rho} {(L(p)^{-1})^{\rho}}_{\sigma} e^{\sigma}(\vec{p}, \pm 1) = {L(\Lambda p)^{\mu}}_{\nu} {W(\Lambda, p)^{\nu}}_{\rho} e^{\rho}(\vec{k}, \pm 1)
\end{equation}
Sappiamo già che per una trasformazione infinitesima \( W \simeq 1 + i a A + i a B + i \theta J_{3} \) dove \( A \) e \( B \) sono tra i generatori di \( \mathrm{ISO}(2) \), quindi
\begin{align}
  {\Lambda^{\mu}}_{\nu} e^{\nu}(\vec{p}, \pm 1) &\simeq {L(\Lambda p)^{\mu}}_{\nu} \left( e^{\nu}(\vec{k}, \pm 1) \pm i\theta e^{\nu}(\vec{k}, \pm 1) + \frac{a \mp i b}{\sqrt{2} \kappa} k^{\nu}\right) \\
                                                &\simeq (1 \pm i \theta) e^{\mu}(\vec{(\Lambda p)}, \pm 1) + \frac{a \mp i b}{\sqrt{2} \kappa} p^{\mu}
\end{align}
Pare che valga la legge di trasformazione in generale
\begin{equation}
    {\Lambda^{\mu}}_{\nu} e^{\nu}(\vec{p}, \pm 1) = e^{\pm i \theta} \left(e^{\mu}(\vec{(\Lambda p)}, \pm 1) + \frac{a \pm i b}{\sqrt{2} \kappa} p^{\mu}\right)
\end{equation}
dove \( a, b, \theta \) sono i parametri del little group. Quindi se costuiamo il campo vettoriale massless come
\begin{equation}
    A_{\mu}(x) = \sum_{h = \pm 1} \int \Dp \left(e_{\mu}(\vec{p}, h) a(\vec{p}, h) e^{i px} +  e_{\mu}^{*}(\vec{p}, h) a^{\dagger}\ped{c}(\vec{p}, h) e^{-ipx}\right)
\end{equation}
non è un quadrivettore sotto trasformazioni di Lorentz ma vale
\begin{equation}
    U_{0}(\Lambda, a) A_{\mu}(x) U_{0}(\Lambda, a)^{-1} = {(\Lambda^{-1})_{\mu}}^{\nu} A_{\nu}(\Lambda x + a) + \partial_{\mu} \alpha(x)
\end{equation}
dove \( \alpha \) è una funzione scalare.


Mostra poi che è invece possibile costruire direttamente un campo a 2 indici antisimmetrico \( F_{\mu\nu} \) che stà quindi nella \( (1, 0) \oplus (0, 1) \) associato alle eccitazioni di particelle massless di elicità \( \pm 1 \). In realtà tale campo è esattamente
\begin{equation}
    F_{\mu\nu}(x) = \partial_{\mu} A_{\nu}(x) - \partial_{\nu} A_{\mu}(x)
\end{equation}
dove \( A_{\mu} \) è quello di sopra, nel senso che la consistenza impone che i vettori di polarizzazione siano
\begin{equation}
    u_{\mu\nu}(\vec{p}, \pm 1) = i p_{\mu} e_{\nu}(\vec{p}, \pm 1) - ip_{\nu} e_{\mu}(\vec{p}, \pm 1)
\end{equation}

Il campo così costruito soddisfa automaticamente le equazioni di Maxwell
\begin{equation}
    \partial^{\mu} F_{\mu\nu} = 0 \qquad \partial_{[\mu} F_{\nu \rho]} = 0
\end{equation}

\subsection{\textcolor{red}{(TODO)} Campo causale generico}

\subsection{\textcolor{red}{(TODO)} \texorpdfstring{\( \mathsf{CPT} \)}{CPT}}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
