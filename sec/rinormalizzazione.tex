\section{Rinormalizzazione}
\subsection{Funzione di Green}
Vogliamo capire un po' meglio la struttura delle singolarità delle ampiezze derivate dai diagrammi di Feynmann. Definiamo
\begin{gather}
    G_{\alpha \to \beta}(x_{1}, \ldots, x_{n}) = \braket{\beta | \T\left\{A_{1}(x_{1}) \cdots A_{n}(x_{n})\right\} | \alpha} \\
    \tilde{G}_{\alpha \to \beta}(p_{1}, \ldots, p_{n}) = \int \dif[4]{x_{1}} \cdots \dif[4]{x_{n}} e^{-ip_{1} \cdot x_{1} - \cdots - i p_{n} \cdot x_{n}} G(x_{1}, \ldots, x_{n})
\end{gather}
dove \( \ket{\alpha} \) e \( \ket{\beta} \) sono stati di singola particella della teoria interagente, \( A_{i} \) sono campi in rappresentazione di Heisenberg. Se gli \( A_{i} \) sono i campi liberi che compaiano nella lagrangiana, allora \( \tilde{G}(p_{1}, \ldots, p_{n}) \) è proprio data dalla somma dei diagrammi di Feynman nel \emph{momentum space} con le linee esterne \emph{off-shell}. In generale però i campi \( A_{i} \) sono delle funzioni locali di campi e loro derivate.

Supponiamo che il sistema sia invariante sotto traslazioni. Allora ogni funzione locale dei campi deve soddisfare
\begin{equation}
    e^{i a_{\mu} P^{\mu}} A_{i}(x) e^{-ia_{\mu} P^{\mu}} = A_{i}(x + a) \qquad \Rightarrow \qquad \left[P_{\mu}, A_{i}(x)\right] = i \partial_{\mu} A_{i}(x)
\end{equation}
Essendo \( e^{i a_{\mu} P^{\mu}} \ket{\alpha} = e^{i a \cdot p_{\alpha}} \ket{\alpha} \) e similmente per \( \ket{\beta} \) si ha
\begin{align*}
  e^{i a \cdot(p_{\beta} - p_{\alpha})}G_{\alpha \to \beta}(x_{1}, \ldots, x_{n}) &= \braket{\beta | \T\left\{e^{i a_{\mu} P^{\mu}}  A_{1}(x_{1}) e^{-i a_{\mu} P^{\mu}} e^{i a_{\mu} P^{\mu}} \cdots e^{-i a_{\mu} P^{\mu}} e^{i a_{\mu} P^{\mu}} A_{n}(x_{n}) e^{-i a_{\mu} P^{\mu}}\right\} | \alpha} \\
                          &= \braket{\beta | \T\left\{A_{1}(x_{1} + a) \cdots A_{n}(x_{n} + a)\right\} | \alpha} \\
  &=  G_{\alpha \to \beta}(x_{1} + a, \ldots, x_{n} + a)
\end{align*}
Se \( \ket{\alpha} \) e \( \ket{\beta} \) sono stati di vuoto, allora \( G \) invariante sotto traslazioni di tutti gli argomenti e quindi la sua trasformata di Fourier è proporzionale a \( \delta^{(4)}(p_{1} + \cdots + p_{n}) \). Nel caso più generale si ha
\begin{align*}
  \tilde{G}_{\alpha \to \beta}(p_{1}, \ldots, p_{n}) &= \int \dif[4]{x_{1}} \cdots \dif[4]{x_{n}} e^{-ip_{1} \cdot x_{1} - \cdots - i p_{n} \cdot x_{n}}  e^{i x_{1} \cdot(p_{\beta} - p_{\alpha})} G_{\alpha \to \beta}(0, x_{2} - x_{1}, \ldots, x_{n} - x_{1}) \\
                                  &= \int \dif[4]{x_{1}} \dif[4]{y_{2}} \cdots \dif{y_{n}} e^{-i (p_{\alpha} + p_{1} + \cdots + p_{n} - p_{\beta}) \cdot x_{1} - i p_{2} - \cdot -i p_{n} \cdot y_{n}} G_{\alpha \to \beta}(0, y_{2}, \ldots, y_{n}) \\
                                  &= (2\pi)^{4} \delta^{(4)}(p_{\alpha} + p_{1}+\cdots+p_{n}-p_{\beta}) \int \dif[4]{y_{2}} \cdots \dif{y_{n}} e^{-i (p_{1} + \cdots p_{n}) \cdot x_{1} - i p_{2} - \cdot -i p_{n} \cdot y_{n}} G_{\alpha \to \beta}(0, y_{2}, \ldots, y_{n}) \\
  &= (2\pi)^{4} \delta^{(4)}(p_{\alpha} + p_{1}+\cdots+p_{n}-p_{\beta}) f_{\alpha \to \beta}(p_{2}, \ldots, p_{n})
\end{align*}
Essendo \( f \) moltiplicate per la \( \delta \) possiamo reintrodurre una dipendenza da \( p_{1} \) e ridefinire la \( \tilde{G}_{\alpha \to \beta} \) nel seguente modo più conveniente
\begin{equation}
    \int \dif[4]{x_{1}} \cdots \dif[4]{x_{n}} e^{-ip_{1} \cdot x_{1} - \cdots - i p_{n} \cdot x_{n}} G_{\alpha \to \beta}(x_{1}, \ldots, x_{n}) = (2\pi)^{4} \delta^{(4)}(p_{\alpha} + p_{1}+\cdots+p_{n}-p_{\beta}) \,\tilde{G}_{\alpha \to \beta}(p_{1}, \ldots, p_{n})
\end{equation}

\subsection{Polologia}
Vogliamo capire un po’ meglio la struttura delle singolarità delle ampiezze derivate dai diagrammi di Feynman, ovvero della \( \tilde{G}_{\alpha \to \beta}(p_{1}, \ldots, p_{n}) \). Consideriamo il caso in cui \( \ket{\alpha} = \ket{\beta} = \ket{\Omega} \) stato di vuoto della teoria interagente e poniamo per semplicità \( G_{\Omega \to \Omega} = G \) e analogamente per la sua trasformata.

Fissiamo un \( 0 \leq r \leq n \) definiamo
\begin{equation}
    q_r = p_{1} + \cdots + p_{r} = - p_{r+1} - \cdots - p_{n}
\end{equation}
dove si intendono le cose ovvie per \( r = 0, n \). Vogliamo mostrare che \( \tilde{G} \) ha un polo in \( {(q_r)}^{2} =  m^{2} \), dove \( m \) è la massa di un qualsiasi stato di singola particella che ha elemento di matrice non nullo con \( A_{1}^{\dagger} \cdots A_{n}^{\dagger} \ket{\Omega} \) e \( A_{1}\cdots A_{n} \ket{\Omega} \).

Tra tutti i possibili \( n! \) ordinamenti di \( x_{1}^{0}, \ldots, x_{n}^{0} \) ce ne sono \( \binom{n}{r} \) in cui i primi \( r \) degli \( x_{i}^{0} \) sono tutti maggiori dei restanti \( n - r \). Isolando questo contributo si ha
\begin{align*}
  G(x_{1}, \ldots, x_{n}) &= \theta(\min\{x_{1}^{0}, \ldots, x_{r}^{0}\} - \min\{x_{r+1}^{0}, \ldots, x_{n}^{0}\}) \\
                          & \qquad \times \braket{\Omega | \T\{A_{1}(x_{1}) \cdots A_{r}(x_{r}) \} \T\{A_{r+1}(x_{r+1}) \cdots A_{n}(x_{n})\} | \Omega} \\
  & \quad + \text{altri termini}
\end{align*}
Inserendo un'identità scritta in termini di autostati della particella di massa \( m \) di prima si ha
\begin{align*}
  G(x_{1}, \ldots, x_{n}) &= \theta(\min\{x_{1}^{0}, \ldots, x_{r}^{0}\} - \min\{x_{r+1}^{0}, \ldots, x_{n}^{0}\}) \\
                          & \qquad \times
                            \sum_{\sigma} \int \dif[3]{\vec{p}}  \braket{\Omega | \T\{A_{1}(x_{1}) \cdots A_{r}(x_{r}) \} | \vec{p}, \sigma} \braket{ \vec{p}, \sigma | \T\{A_{r+1}(x_{r+1}) \cdots A_{n}(x_{n})\} | \Omega} \\
  & \quad + \text{altri termini}
\end{align*}
Usando ora le proprietà di trasformazione sotto traslazione delle funzioni di Green si ha
\begin{align*}
  \braket{\Omega | \T\{A_{1}(x_{1}) \cdots A_{r}(x_{r}) | \vec{p}, \sigma} = e^{i p \cdot x_{1}} \braket{\Omega | \T\{A_{1}(0) A_{2}(x_{2} - x_{1}) \cdots A_{r}(x_{r} - x_{1}) | \vec{p}, \sigma}
\end{align*}
e analogamente
\begin{equation*}
    \braket{\vec{p}, \sigma | \T\{A_{r+1}(x_{r+1}) \cdots A_{n}(x_{n}) | \Omega} = e^{-ip \cdot x_{r+1}} \braket{\Omega | \T\{A_{r}(0) A_{r+2}(x_{r+2} - x_{r+1}) \cdots A_{n}(x_{n} - x_{r+1}) | \vec{p}, \sigma}
\end{equation*}
Facciamo il cambio di variabile \( y_{i} = x_{i} - x_{1} \) per \( 2 \leq i \leq r \) e \( y_{i} = x_{i} - x_{r} \) per \( r+2 \leq i \leq n \). L'argomento della \( \theta \) si riscrive come
\begin{equation*}
    \min\{x_{1}^{0}, \ldots, x_{r}^{0}\} - \min\{x_{r+1}^{0}, \ldots, x_{n}^{0}\} = x_{1}^{0} - x_{r+1}^{0} + \min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}
\end{equation*}
da cui, posto
\begin{gather*}
    \mathcal{M}_{\ket{\Omega} \to \ket{\vec{p}, \sigma}} (y_{2}, \ldots, y_{r}) = \braket{\Omega | \T\{A_{1}(0) A_{2}(y_{2})\cdots A_{r}(y_{r}) \} | \vec{p}, \sigma} \\
    \mathcal{M}_{\ket{\vec{p}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) = \braket{ \vec{p}, \sigma | \T\{A_{r+1}(0) A_{r+2}(y_{r+2}) \cdots A_{n}(y_{n})\} | \Omega}
\end{gather*}

\begin{align*}
  G(x_{1}, \ldots, x_{n}) &= \theta(x_{1}^{0} - x_{r+1}^{0} + \min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}) \\
  &\qquad \times \sum_{\sigma} \int \dif[3]{\vec{p}} \,  e^{i p \cdot(x_{1} - x_{r+1})} \mathcal{M}_{\ket{\Omega} \to \ket{\vec{p}, \sigma}} (y_{2}, \ldots, y_{r}) \mathcal{M}_{\ket{\vec{p}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) \\
  & \quad + \text{altri termini}
\end{align*}
Usando ora la rappresentazione integrale della \( \theta \)
\begin{equation}
    \theta(x) = - \frac{1}{2\pi i} \int_{\R} \frac{e^{-ik x}}{k + i\epsilon} \dif{k} \qquad (\epsilon \to 0^{+})
\end{equation}
si ha
\begin{align*}
  G(x_{1}, \ldots, x_{n}) &= - \frac{1}{2\pi i} \sum_{\sigma } \int \dif{k^{0}} \int \dif[3]{\vec{p}} \,
                            e^{i(p^{0} - k^{0}) x_{1}^{0} - i(p^{0} - k^{0}) x_{r+1}^{0}} e^{-i \vec{p} \cdot (\vec{x}_{1} - \vec{x}_{r+1})} \, e^{-ik^0\left(\min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}\right)} \\
  & \qquad \times \frac{1}{k^0 +  i \epsilon} \mathcal{M}_{\ket{\Omega} \to \ket{\vec{p}, \sigma}} (y_{2}, \ldots, y_{r}) \mathcal{M}_{\ket{\vec{p}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) \\
  & \quad + \text{altri termini}
\end{align*}
ove \( p^{0} = \sqrt{\vec{p}^{2} + m^{2}} \). Passando ora in trasformata negli impulsi e integrando in \( x_{1} \) e \( x_{r+1} \)
\begin{align*}
  \tilde{G}'(p_{1}, \ldots, p_{n}) &= \int \dif[4]{x_{1}} \cdots \dif[4]{x_{n}} e^{-i p_{1} \cdot x_{1} - \cdots - i p_{n} \cdot x_{n}} G(x_{1}, \dots, x_{n}) \\
  & = \int \dif[4]{x_{1}}\dif[4]{y_{2}} \cdots \dif[4]{y_{r}} \dif[4]{x_{r+1}}\dif[4]{y_{r+2}} \cdots \dif[4]{y_{n}} \\
                                   & \qquad \times e^{-i(p_{1} + \cdots + p_{r}) \cdot x_{1}} e^{- i (p_{r+1} + \cdots + p_{n}) \cdot x_{r+1}} e^{-i p_{2} \cdot y_{2} - \cdots -i p_{r} y_{r} - i p_{r+2} \cdots y_{r+2} - \cdots -i p_{n} \cdot y_{n}} \\
  & \qquad \times G(x_{1}, x_{1} + y_{2}, \ldots, x_{1} + y_{r}, x_{r+1}, x_{r+1} + y_{r+1}, \ldots, x_{r+1} + y_{n})
\end{align*}
Inserendo l'espressione precedente e integrando in \( x_{1} \) e \( x_{r+1} \) otteniamo allora
\begin{align*}
  \tilde{G}'(p_{1}, \ldots, p_{n})
  &= - \frac{(2\pi)^{8}}{2\pi i} \sum_{\sigma } \int \dif{k^0} \int \dif[3]{\vec{p}} \int \dif[4]{y_{2}} \cdots \dif[4]{y_{r}} \dif[4]{y_{r+2}} \cdots \dif[4]{y_{n}} e^{-ik^0\left(\min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}\right)} \\
  &\qquad \times \delta(p^{0} - k^{0} - p_{1}^{0} -\cdots -p_{r}^{0}) \delta^{(3)}(\vec{p} - \vec{p}_{1} - \cdots - \vec{p}_{r}) \\
  &\qquad \times \delta(p^{0} - k^{0} + p_{r+1}^{0} +\cdots +p_{n}^{0}) \delta^{(3)}(\vec{p} + \vec{p}_{r+1} + \cdots + \vec{p}_{n}) \\
  & \qquad \times \frac{1}{k^0 +  i \epsilon} \mathcal{M}_{\ket{\Omega} \to \ket{\vec{p}, \sigma}} (y_{2}, \ldots, y_{r}) \mathcal{M}_{\ket{\vec{p}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) \\
  & \quad + \text{altri termini}
\end{align*}
Integrando in \( \vec{p} \) otteniamo
\begin{align*}
  \tilde{G}'(p_{1}, \ldots, p_{n})
  &= i (2\pi)^{7} \delta^{(3)}(\vec{p}_{1} + \cdots + \vec{p}_{n}) \sum_{\sigma } \int \dif{k^0} \int \dif[4]{y_{2}} \cdots \dif[4]{y_{r}} \dif[4]{y_{r+2}} \cdots \dif[4]{y_{n}} e^{-ik^0\left(\min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}\right)} \\
  &\qquad \times \delta{\left(\sqrt{{(\vec{p}_{1} + \cdots + \vec{p}_{r})}^{2} + m^{2}} - k^{0} - p_{1}^{0} - \cdots - p_{r}^{0}\right)} \\
  &\qquad \times \delta{\left(\sqrt{{(\vec{p}_{1} + \cdots + \vec{p}_{r})}^{2} + m^{2}} - k^{0} + p_{r+1}^{0} + \cdots + p_{n}^{0}\right)} \\
  & \qquad \times \frac{1}{k^0 +  i \epsilon} \mathcal{M}_{\ket{\Omega} \to \ket{\vec{p}_{1} + \cdots + \vec{p}_{r}, \sigma}} (y_{2}, \ldots, y_{r}) \mathcal{M}_{\ket{\vec{p}_{1} + \cdots + \vec{p}_{r}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) \\
  & \quad + \text{altri termini}
\end{align*}
Integrando in \( k^{0} \)
\begin{align*}
  \tilde{G}'(p_{1}, \ldots, p_{n})
  &= i (2\pi)^{7} \delta^{(3)}(\vec{p}_{1} + \cdots + \vec{p}_{n}) \delta(p_{1}^{0} + \cdots + p_{n}^{0})  \\
  & \qquad \times \sum_{\sigma }  \int \dif[4]{y_{2}} \cdots \dif[4]{y_{r}} \dif[4]{y_{r+2}} \cdots \dif[4]{y_{n}} e^{-i\left[\sqrt{{(\vec{p}_{1} + \cdots + \vec{p}_{r})}^{2} + m^{2}} - p_{1}^{0} - \cdots - p_{r}^{0}\right]\left(\min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}\right)} \\
  & \qquad \times \frac{1}{\sqrt{{(\vec{p}_{1} + \cdots + \vec{p}_{r})}^{2} + m^{2}} - p_{1}^{0} - \cdots - p_{r}^{0} +  i \epsilon} \\
  &\qquad \times \mathcal{M}_{\ket{\Omega} \to \ket{\vec{p}_{1} + \cdots + \vec{p}_{r}, \sigma}} (y_{2}, \ldots, y_{r}) \mathcal{M}_{\ket{\vec{p}_{1} + \cdots + \vec{p}_{r}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) \\
  & \quad + \text{altri termini}
\end{align*}
Posto quindi \( q_{r} = p_{1} + \cdots + p_{r} \) e \( \tilde{G}'(p_{1}, \ldots, p_{n}) = (2\pi)^{4} \delta^{(4)}(p_{1} + \cdots + p_{n}) \tilde{G}(p_{1}, \ldots, p_{n}) \) abbiamo ricavato
\begin{align*}
  \tilde{G}(p_{1}, \ldots, p_{n}) &= \frac{i (2\pi)^{3}}{\sqrt{\vec{q}_{r}^{2} + m^{2}} - q_{r}^{0} + i \epsilon} \\
                                  &\qquad \times \sum_{\sigma} \int \dif[4]{y_{2}} \cdots \dif[4]{y_{r}} \dif[4]{y_{r+2}} \cdots \dif[4]{y_{n}} e^{-i\left[\sqrt{\vec{q}_{r}^{2} + m^{2}} - q_{r}^{0}\right]\left(\min\{0, y_{2}^{0}, \ldots, y_{r}^{0}\} - \min\{0, y_{r+2}^{0}, \ldots, y_{n}^{0}\}\right)} \\
                                  &\qquad \times  \mathcal{M}_{\ket{\Omega} \to \ket{\vec{q}_{r}, \sigma}} (y_{2}, \ldots, y_{r}) \mathcal{M}_{\ket{\vec{q}_{r}, \sigma} \to \ket{\Omega}} (y_{r+1}, \ldots, y_{n}) \\
  &\quad + \text{altri termini}
\end{align*}
Siamo interessati nell'andamento della funzione di Green vicino al polo \( q_{r}^{0} = \sqrt{\vec{q}_{r}^{2} + m^{2}} \), ovvero \( q_{r}^{2} = m^{2} \). Il pre-fattore si riscrive, per \( \epsilon \to 0 \) e vicino al polo si riscrive come
\begin{equation*}
    \frac{1}{\sqrt{\vec{q}_{r}^{2} + m^{2}} - q_{r}^{0} + i \epsilon} = \frac{\sqrt{\vec{q}_{r}^{2} + m^{2}} + q_{r}^{0} + i \epsilon}{\left(\sqrt{\vec{q}_{r}^{2} + m^{2}} + i \epsilon\right)^{2} - (q_{r}^{0})^{2}} \to - \frac{2 \sqrt{\vec{q_{r}}^{2} + m^{2}}}{q^{2} - m^{2} - i\epsilon}
\end{equation*}
dove abbiamo ridefinito \( 2 \epsilon \sqrt{{\vec{q}_{r}}^{2} + m^{2}} \to \epsilon \). Vicino al polo possiamo anche trascurare l'esponenziale nell'integrale e gli ``altri termini'' che sono regolari vicino al polo. Di conseguenza per \( q^{2} \to m^{2} \) otteniamo
\begin{equation}
    \label{eq:polo-green-trasformata}
    \tilde{G}(p_{1}, \ldots, p_{n}) \to - \frac{2i\sqrt{\vec{q_{r}}^{2} + m^{2}}}{q^{2} - m^{2} - i\epsilon} (2\pi)^{3}\sum_{\sigma} \mathcal{M}_{\ket{\Omega} \to \ket{\vec{q}_{r}, \sigma}} \mathcal{M}_{\ket{\vec{q}_{r}, \sigma} \to \ket{\Omega}}
\end{equation}
ove
\begin{align}
    \mathcal{M}_{\ket{\Omega} \to \ket{\vec{q}_{r}, \sigma}} &=  \int \dif[4]{y_{2}} \cdots \dif[4]{y_{r}} \braket{\Omega | \T\{A_{1}(0) A_{2}(y_{2})\cdots A_{r}(y_{r}) \} | \vec{q}_{r}, \sigma} \\
    \mathcal{M}_{\ket{\vec{q}_{r}, \sigma} \to \ket{\Omega}} =& \int \dif[4]{y_{r+2}} \cdots \dif[4]{y_{n}}\braket{ \vec{q}_{r}, \sigma | \T\{A_{r+1}(0) A_{r+2}(y_{r+2}) \cdots A_{n}(y_{n})\} | \Omega}
\end{align}

\subsection{Rinormalizzazione del campo e di massa}
Consideriamo il caso in cui \( A_{1} = \Phi_{l} \) sia un campo che trasforma all'interno di una rappresentazione irriducibile finito-dimensionale di Lorentz. Nella derivazione dei campi liberi avevamo visto che le proprietà di trasformazione fissavano la forma del campo a parte per costanti globali nel coefficienti \( u_{l}, v_{l} \); pertanto dire che \( \Phi_{l} \) trasforma in modo irriducibile, implica che sia della forma
\begin{equation}
    \Phi_{l}(x) = \sum_{\sigma} \int \Dp \left( N u_{l}(\vec{p}, \sigma) a(\vec{p},\sigma) e^{i p \cdot x} + N' v_{l}(\vec{p}, \sigma) a^{\dagger}(\vec{p},\sigma) e^{-i p \cdot x}\right)
\end{equation}
con \( N, N' \) costanti e dove i coefficienti \( u_{l} \) e \( v_{l} \) sono quelli dei campi liberi.

Prendendo ora il caso \( r=1 \) nella~\eqref{eq:polo-green-trasformata}
\begin{equation}
    \tilde{G}_{l}(p_{1}, \ldots, p_{n}) \to - \frac{2i\sqrt{\vec{p}_{1}^{2} + m^{2}}}{p_{1}^{2} - m^{2} - i\epsilon} (2\pi)^{3} \sum_{\sigma} \braket{\Omega | \Phi_{l}(0) | \vec{p}_{1}, \sigma} \mathcal{M}_{\ket{\vec{p}_{1}, \sigma} \to \ket{\Omega}}
\end{equation}
essendo \( q_{1} = p_{1} \) e dove
\begin{equation}
    \mathcal{M}_{\ket{\vec{p}_{1}, \sigma} \to \ket{\Omega}} = \int \dif[4]{y_{3}} \cdots \dif[4]{y_{n}}\braket{ \vec{p}_{1}, \sigma | \T\{A_{2}(0) A_{3}(y_{3}) \cdots A_{n}(y_{n})\} | \Omega}
\end{equation}
Usando la forma del campo si ha
\begin{equation}
    \braket{\Omega | \Phi_{l}(0) | \vec{p}_{1}, \sigma} = \sum_{\sigma'} \int \Dp N u_{l}(\vec{p}, \sigma) \braket{\vec{p}, \sigma' | \vec{p}_{1}, \sigma} = \frac{N}{(2\pi)^{3/2}} u_{l}(\vec{p}, \sigma)
\end{equation}
Supponiamo inoltre di poter riscrivere
\begin{equation}
     \mathcal{M}_{\ket{\vec{p}_{1}, \sigma} \to \ket{\Omega}} = \frac{1}{(2\pi)^{3/2}N} \sum_{l} u_{l}^{*}(\vec{p}_{1}, \sigma) \mathcal{M}_{l}
\end{equation}
allora
\begin{equation}
    \tilde{G}_{l}(p_{1}, \ldots, p_{n}) \to - \frac{2i\sqrt{\vec{p}_{1}^{2} + m^{2}}}{p_{1}^{2} - m^{2} - i\epsilon} \sum_{\sigma, l'} u_{l}(\vec{p}_{1}, \sigma) u_{l'}^{*}(\vec{p}_{1}, \sigma) \mathcal{M}_{l'}
\end{equation}
Il termine che moltiplica \( \mathcal{M}_{l} \) sommato su \( \sigma \) è proprio il propagatore del campo libero di massa \( m \) con le stesse proprietà di trasformazione sotto Loretz di \( \Phi_{l} \)
\begin{equation}
    \sum_{\sigma} u_{l}(\vec{p}, \sigma) u_{l'}^{*}(\vec{p}, \sigma) = -i \Delta_{ll'}(p)
\end{equation}

\subsection{Funzionali rinormalizzati}
Facciamo nella teoria \( \phi^{4} \), non so quanto si estende facilmente ad altro. La lagrangiana è
\begin{equation}
    \mathscr{L} = \frac{1}{2}(\partial_{\mu} \phi\ped{B})^{2} - \frac{1}{2} m\ped{B}^{2} \phi\ped{B}^{2} - \frac{g\ped{B}}{4!} \phi\ped{B}^{4}
\end{equation}
Definiamo i campi e le costanti rinormalizzate come
\begin{equation}
    \phi\ped{B} = Z_{\phi}^{1/2} \phi\ped{R} \qquad m\ped{B}^{2} = \frac{Z_{m}}{Z_{\phi}} m\ped{R}^{2} \qquad g\ped{B} = \frac{Z_{g}}{Z_{\phi}^{2}} g\ped{R}
\end{equation}
Così la lagrangiana diventa
\begin{equation}
    \mathscr{L} = \frac{1}{2} Z_{\phi}(\partial_{\mu} \phi\ped{R})^{2} - \frac{1}{2} Z_{m} m\ped{R}^{2} \phi\ped{R}^{2} - \frac{Z_{g} g\ped{R}}{4!} \phi\ped{R}^{4}
\end{equation}

Le relazioni tra funzionali bare e rinormalizzati sono
\begin{align}
  W\ped{R}[J_{R}; m\ped{R}^{2}, g\ped{R}] &= W\ped{B}\left[Z_{\phi}^{-1/2} J\ped{R};  \frac{Z_{m}}{Z_{\phi}} m\ped{R}^{2}, \frac{Z_{g}}{Z_{\phi}^{2}} g\ped{R}\right] \\
  \Gamma\ped{R}[\varphi\ped{R}; m\ped{R}^{2}, g\ped{R}] &= \Gamma\ped{B}\left[Z_{\phi}^{1/2} \varphi\ped{R};  \frac{Z_{m}}{Z_{\phi}} m\ped{R}^{2}, \frac{Z_{g}}{Z_{\phi}^{2}} g\ped{R}\right]
\end{align}
Per cui le derivate funzionali \( n \)-esime soddisfano
\begin{align}
  \tilde{G}\ped{c, R}^{(n)}(p_{i}; m\ped{R}^{2}, g\ped{R}) &= Z_{\phi}^{-n/2} \tilde{G}\ped{c, B}^{(n)}\left(p_{i}; \frac{Z_{m}}{Z_{\phi}} m\ped{R}^{2}, \frac{Z_{g}}{Z_{\phi}^{2}} g\ped{R}\right) \\
  \tilde{\Gamma}\ped{R}^{(n)}(p_{i}; m\ped{R}^{2}, g\ped{R}) &= Z_{\phi}^{n/2} \tilde{\Gamma}\ped{B}^{(n)}\left(p_{i}; \frac{Z_{m}}{Z_{\phi}} m\ped{R}^{2}, \frac{Z_{g}}{Z_{\phi}^{2}} g\ped{R}\right)
\end{align}

\subsection{Callan-Symanzik e running coupling}
In regolarizzazione dimensionale poniamo
\begin{equation}
    g\ped{B} = \mu_{0}^{\epsilon} \tilde{g}\ped{B}(\mu_{0}) \qquad g\ped{R} = \mu^{\epsilon} \tilde{g}\ped{R}(\mu) \qquad \Rightarrow \qquad \tilde{g}\ped{R}(\mu_{0}) = \left(\frac{\mu}{\mu_{0}}\right)^{\epsilon} \frac{Z_{g}}{Z_{\phi}^{2}} \tilde{g}\ped{R}(\mu)
\end{equation}
con \( \tilde{g}\ped{B} \) e \( \tilde{g}\ped{R} \) adimensionali. La \( \tilde{\Gamma}^{(n)}\ped{R} \) acquista una dipendenza esplicita da \( \mu \)
\begin{equation}
    \tilde{\Gamma}^{(n)}\ped{R} = \tilde{\Gamma}^{(n)}\ped{R}(p_{i}; \mu, m\ped{R}^{2}(\mu), \tilde{g}\ped{R}(\mu))
\end{equation}
Dato che la \( \tilde{\Gamma}\ped{B}^{(n)} \) non dipende da \( \mu \) (ma solo da \( \mu_{0} \)) allora
\begin{equation}
    0 = \mu \dod{}{\mu} ( Z_{\phi}^{-n/2} \tilde{\Gamma}\ped{R}^{(n)} ) = - \frac{n}{2} \mu \dod{\log{Z_{\phi}}}{\mu} Z_{\phi}^{-n/2}\tilde{\Gamma}\ped{R}^{(n)} + Z_{\phi}^{-n/2} \mu \left(\dpd{}{\mu} + \frac{ \dif m\ped{R}^{2}}{ \dif \mu}  \frac{\partial}{\partial m\ped{R}^{2}} + \dod{\tilde{g}\ped{R}}{\mu} \dpd{}{\tilde{g}\ped{R}}\right) \tilde{\Gamma}^{(n)}\ped{R}
\end{equation}
ovvero, posto
\begin{equation}
    \eta =  \mu \dod{\log{Z_{\phi}}}{\mu}  \qquad m\ped{R}^{2}\gamma_{m} = \mu \dod{m\ped{R}^{2}}{\mu} \qquad \beta = \mu \dod{\tilde{g}\ped{R}}{\mu}
\end{equation}
si ha
\begin{equation}
    \left[\mu \dpd{}{\mu} + \beta(\tilde{g}\ped{R}) \dpd{}{\tilde{g}\ped{R}} +\gamma_{m}(\tilde{g}\ped{R})  m\ped{R}^{2} \frac{\partial}{\partial m\ped{R}^{2}} - \frac{n}{2} \eta(\tilde{g}\ped{R})\right] \tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}, \tilde{g}\ped{R}) = 0
\end{equation}
dove abbiamo supposto che \( \gamma_{m}, \eta, \beta \) dipendano unicamente da \( \mu \) tramite \( \tilde{g}\ped{R} \) (cosa che dovrebbe essere vera in \( \phi^{4} \)). Tale equazione è detta \emph{equazione di Callan-Symanzik}.

Risolviamo tale equazione con il metodo delle caratteristiche. Affermo che una soluzione è della forma
\begin{equation}
    \tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}(\mu), \tilde{g}\ped{R}(\mu)) = \exp\left(\frac{n}{2} \int_{\mu'}^{\mu} \eta(g\ped{R}(\xi)) \frac{\dif\xi}{\xi}\right) \tilde{\Gamma}^{(n)}(p_{i}; \mu', (m\ped{R}')^{2}, \tilde{g}\ped{R}')
\end{equation}
dove \( \tilde{\Gamma}^{(n)}(p_{i}; \mu', (m\ped{R}')^{2}, \tilde{g}\ped{R}') = f(\mu') \) è la condizione iniziale assegnata e \( m\ped{R}^{2}(\mu) \) e \( \tilde{g}\ped{R}(\mu) \) sono i \emph{running couplings}, ovvero le soluzioni delle equazioni differenziali
\begin{equation}
    \begin{dcases}
        \mu \dod{\tilde{g}\ped{R}}{\mu} = \beta(\tilde{g}\ped{R}(\mu)) \\
        \mu \dod{m\ped{R}^{2}}{\mu} = m\ped{R}^{2} \gamma_{m}(\tilde{g}\ped{R}(\mu)) \\
        \tilde{g}\ped{R}(\mu') = \tilde{g}\ped{R}', \quad m\ped{R}(\mu') = m\ped{R}'
    \end{dcases}
\end{equation}
Per verificarlo basta osservare che
\begin{align}
  0 &= \mu \dod{}{\mu} \tilde{\Gamma}^{(n)}(p_{i}; \mu', (m\ped{R}')^{2}, \tilde{g}\ped{R}') \\
    &= \mu \dod{}{\mu} \left[\tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}(\mu), \tilde{g}\ped{R}(\mu)) e^{-\frac{n}{2} \int_{\mu'}^{\mu} \eta(g\ped{R}(\xi)) \frac{\dif\xi}{\xi}}\right] \\
  &= \left[\mu \dpd{}{\mu} + \beta(\tilde{g}\ped{R}) \dpd{}{\tilde{g}\ped{R}} +\gamma_{m}(\tilde{g}\ped{R})  m\ped{R}^{2} \frac{\partial}{\partial m\ped{R}^{2}} - \frac{n}{2} \eta(\tilde{g}\ped{R})\right] \tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}, \tilde{g}\ped{R})
\end{align}

Usiamo ora tale soluzione formale per trovare l'espressione della \( \tilde{\Gamma}^{(n)} \) negli impulsi riscalati. Essendo \( \tilde{\Gamma}^{(n)} \) dimensonale, è una funzione omogenea dei parametri dimensionali, ovvero vale
\begin{equation}
    \tilde{\Gamma}^{(n)}(s p_{i}; s \mu, s^{2} m\ped{R}^{2}, \tilde{g}\ped{R}) = s^{[\tilde{\Gamma}^{(n)}]} \, \tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}, \tilde{g}\ped{R})
\end{equation}
dove \( [\tilde{\Gamma}^{(n)}] \) è la dimensione in massa di \( \tilde{\Gamma}^{(n)} \). Pertanto
\begin{align}
  \tilde{\Gamma}^{(n)}(sp_{i}; \mu, (m\ped{R}^{0})^{2}, \tilde{g}\ped{R}^{0})
  &= s^{[\tilde{\Gamma}^{(n)}]} \, \tilde{\Gamma}^{(n)}\left(p_{i}; \frac{\mu}{s}, \frac{(m\ped{R}^{0})^{2}}{s^{2}}, \tilde{g}\ped{R}^{0}\right) \\
  &=  s^{[\tilde{\Gamma}^{(n)}]} \, \exp\left(-\frac{n}{2} \int_{\mu/s}^{\mu} \eta(\tilde{g}\ped{R}(\xi)) \frac{\dif{\xi}}{\xi}\right) \tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}(\mu), \tilde{g}\ped{R}(\mu))
\end{align}
dove \( m\ped{R}^{2}(\mu), \tilde{g}\ped{R}(\mu) \) sono le costanti di accoppiamento running con condizione iniziale
\begin{equation}
    m\ped{R}\left(\frac{\mu}{s}\right) = m\ped{R}^{0} \qquad \tilde{g}\ped{R}\left(\frac{\mu}{s}\right) = \tilde{g}\ped{R}^{0}
\end{equation}

Pertanto la \( \tilde{\Gamma}^{(n)} \) alla scala \( \mu \), con costante di accoppiamento \( \tilde{g}\ped{R}^{0} \) e agli impulsi riscalati è proporzionale alla \( \tilde{\Gamma}^{(n)} \) alla stessa scala \( \mu \) e agli impulsi iniziali ma con costante di accoppiamento runnata da \( \tilde{g}\ped{R}^{(0)} \) alla scala \( \mu / s \) fino alla scala \( \mu \).

Riscriviamo tale relazione un running fatto con \( s \) invece che con \( \mu \). Definiamo temporaneamente e per chiarezza di notazione
\begin{equation}
    h\ped{R}(\xi) = \tilde{g}\ped{R} \left(\frac{\xi}{s} \mu\right) \qquad M\ped{R} = m\ped{R}\left(\frac{\xi}{s} \mu\right)
\end{equation}
Osserviamo che
\begin{equation}
    \xi \dod{h\ped{R}}{\xi} = \xi \frac{s}{\mu} \dod{\tilde{g}\ped{R}}{\mu} \left(\frac{\xi}{s} \mu\right) = \xi \frac{s}{\mu} \frac{s}{\xi \mu} \beta\left(\tilde{g}\ped{R}\left(\frac{\xi}{s} \mu\right)\right) = \beta(h\ped{R}(\xi))
\end{equation}
Così
\begin{equation}
    \tilde{\Gamma}^{(n)}(sp_{i}; \mu, (m\ped{R}^{0})^{2}, \tilde{g}\ped{R}^{0})
    = s^{[\tilde{\Gamma}^{(n)}]} \, \exp\left(-\frac{n}{2} \int_{1}^{s} \eta(h\ped{R}(\xi)) \frac{\dif{\xi}}{\xi}\right) \tilde{\Gamma}^{(n)}(p_{i}; \mu, M\ped{R}^{2}(s), h\ped{R}(s))
\end{equation}
ovvero tornando a \( h\ped{R} \to \tilde{g}\ped{R}, M\ped{R} \to m\ped{R} \)
\begin{equation}
    \tilde{\Gamma}^{(n)}(sp_{i}; \mu, (m\ped{R}^{0})^{2}, \tilde{g}\ped{R}^{0})
    = s^{[\tilde{\Gamma}^{(n)}]} \, \exp\left(-\frac{n}{2} \int_{1}^{s} \eta(\tilde{g}\ped{R}(\xi)) \frac{\dif{\xi}}{\xi}\right) \tilde{\Gamma}^{(n)}(p_{i}; \mu, m\ped{R}^{2}(s), \tilde{g}\ped{R}(s))
\end{equation}
dove ora si intende che il running è dato da
\begin{equation}
    \begin{dcases}
        s \dod{\tilde{g}\ped{R}}{s} = \beta(\tilde{g}\ped{R}(s)) \\
        s \dod{m\ped{R}^{2}}{s} = m\ped{R}^{2}(s) \gamma_{m}(\tilde{g}\ped{R}(s)) \\
        \tilde{g}\ped{R}(1) = \tilde{g}\ped{R}^{0}, \quad m\ped{R}(1) = m\ped{R}^{0}
    \end{dcases}
\end{equation}

\subsection{\texorpdfstring{\( \beta \)}{beta} function}
Definiamo la \( \beta \)-function come
\begin{equation}
    \beta = \mu \dod{\tilde{g}\ped{R}}{\mu}
\end{equation}
e assumiamo che sia \( \beta(\mu) = f(\tilde{g}\ped{R}(\mu)) \) e quindi nel seguito indichiamo \( \beta(\mu) \to \beta(\tilde{g}\ped{R}(\mu)) \) con abuso di notazione. Usando che
\begin{equation}
    o = \mu \dod{}{\mu}\left(\mu_{0}^{\epsilon} \tilde{g}\ped{B}(\mu_{0})\right) = \mu \dod{}{\mu}\left(\mu^{\epsilon} \frac{Z_{g}}{Z_{\phi}^{2}} \tilde{g}\ped{R}\right)
\end{equation}
si trova che
\begin{equation}
    \beta(\tilde{g}\ped{R}) = - \frac{\epsilon \tilde{g}\ped{R}}{1 + \tilde{g}\ped{R} \dod{\log Z_{g}}{\tilde{g}\ped{R}} - 2 \tilde{g}\ped{R} \dod{\log Z_{\phi}}{\tilde{g}\ped{R}}}
\end{equation}
Perturbativamente si trova per \( \phi^{4} \)
\begin{equation}
    Z_{g} = 1 + \frac{\beta_{0}}{\epsilon} \tilde{g}\ped{R} + O(\tilde{g}\ped{R}) \qquad Z_{\phi} = 1 + O(\tilde{g}\ped{R}^{2})
\end{equation}
quindi
\begin{equation}
    \beta(\tilde{g}\ped{R}) = - \epsilon \tilde{g}\ped{R} + \beta_{0} \tilde{g}\ped{R}^{2} + O(\tilde{g}\ped{R}^{3})
\end{equation}
Per cui nel limite \( \epsilon \to 0 \)
\begin{equation}
    \beta(\tilde{g}\ped{R}) = \beta_{0} \tilde{g}\ped{R}^{2} + O(\tilde{g}\ped{R}^{3})
\end{equation}
Per cui se \( \beta_{0} > 0 \) si ha un \emph{polo di Landau}, mentre se \( \beta_{0} < 0 \) si ha la \emph{libertà asintotica}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
